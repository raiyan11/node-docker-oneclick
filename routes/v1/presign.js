const express = require('express');
const router = express.Router();
const joiMiddleware = require('../../middlewares/joi.middleware');
const joiSchemas = require('../../lib/utils/joi.schemas');
const userRoleMiddleWare = require('../../middlewares/user.role.middleware');
const constants = require('../../config/constants');
const passportMiddleWare = require('../../middlewares/passport.middleware');

const presignComponent = require('../../components/v1/presign/presign.url');
router.patch('/', presignComponent);
module.exports = router;
