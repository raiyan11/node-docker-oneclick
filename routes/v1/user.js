const express = require('express');
const router = express.Router();
const passportMiddleware = require('../../middlewares/passport.middleware');
const joiMiddleware = require('../../middlewares/joi.middleware');
const joiSchemas = require('../../lib/utils/joi.schemas');
const verifiedMiddleware = require('../../middlewares/verified.middleware');

const getUserComponent = require('../../components/v1/user/get.user');
const updateUserComponent = require('../../components/v1/user/update.user');
const deleteUserComponent = require('../../components/v1/user/delete.user');
const getOtherUserComponent = require('../../components/v1/user/fetch.other.user');
const blockUserComponent = require('../../components/v1/user/block.user');
const unblockUserComponent = require('../../components/v1/user/unblock.user');
const userPolicyComponent = require('../../components/v1/user/policy.user');
const userUnfollowComponent = require('../../components/v1/follows/unfollow.user');

// User hashtag
const getUserHashtagsComponent = require('../../components/v1/user/user.hashtags.get');
const userHashtagPreferenceComponent = require('../../components/v1/user/user.hashtags.set');
const userHashtagSuggestComponent = require('../../components/v1/user/suggest.user.hashtag');

// follow user
const followUserComponent = require('../../components/v1/follows/follow.user');
const acceptFollowRequestComponent = require('../../components/v1/follows/accept.request');
const rejectFollowRequestComponent = require('../../components/v1/follows/reject.request');
const getFollowersComponent = require('../../components/v1/follows/get.followers');
const getFollowingComponent = require('../../components/v1/follows/get.following');
const getOtherFollowersComponent = require('../../components/v1/follows/get.other.followers');
const getOtherFollowingComponent = require('../../components/v1/follows/get.other.following');
const postapprovalComponent = require('../../components/v1/user/user.post.approval');
const deactivateAccountComponent = require('../../components/v1/user/deactivate.user');
const activeAccountComponent = require('../../components/v1/user/activate.user');
const approveAccountComponent = require('../../components/v1/user/user.approve');
const getBlockedUserComponent = require('../../components/v1/user/get.blocked.list');
const getUserByUserName = require('../../components/v1/user/get.by.username')

router.post('/approval/code', passportMiddleware.jwtAuth, approveAccountComponent);
router.put('/deactivate', passportMiddleware.jwtAuth, deactivateAccountComponent);
router.put('/activate', passportMiddleware.jwtAuth, activeAccountComponent);
router.get('/followers', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), getFollowersComponent);
router.get('/following', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), getFollowingComponent);
router.get('/settings/hashtag', passportMiddleware.jwtAuth, getUserHashtagsComponent);
router.post('/approval', passportMiddleware.jwtAuth, joiMiddleware.joiBodyMiddleware(joiSchemas.userApproval, 'approval'), verifiedMiddleware.accoutDeactivated(), postapprovalComponent);
router.put('/settings/hashtag', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), joiMiddleware.joiBodyMiddleware(joiSchemas.userHashtagPreferences, 'hashtags'), userHashtagPreferenceComponent);
router.post('/settings/hashtag/suggest', passportMiddleware.jwtAuth,verifiedMiddleware.accoutDeactivated(), joiMiddleware.joiBodyMiddleware(joiSchemas.userHashtagSuggesh, 'hashtag'), userHashtagSuggestComponent);
router.put('/settings/post', passportMiddleware.jwtAuth, joiMiddleware.joiBodyMiddleware(joiSchemas.userPolicy),verifiedMiddleware.accoutDeactivated(), userPolicyComponent);
router.put('/:id/block', passportMiddleware.jwtAuth,verifiedMiddleware.accoutDeactivated(), blockUserComponent);
router.put('/:id/unblock', passportMiddleware.jwtAuth,verifiedMiddleware.accoutDeactivated(), unblockUserComponent);
router.put('/:id/unfollow', passportMiddleware.jwtAuth,verifiedMiddleware.accoutDeactivated(), userUnfollowComponent);
router.get('/block', passportMiddleware.jwtAuth,verifiedMiddleware.accoutDeactivated(), getBlockedUserComponent);
router.get('/username/:name', passportMiddleware.optionalJwtAuth,getUserByUserName);
router.get('/:id/followers', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), getOtherFollowersComponent);
router.get('/:id/following', passportMiddleware.jwtAuth,  verifiedMiddleware.accoutDeactivated(), getOtherFollowingComponent);
router.get('/:id', passportMiddleware.optionalJwtAuth, getOtherUserComponent);
router.get('/', passportMiddleware.jwtAuth, getUserComponent);
router.put('/', passportMiddleware.jwtAuth, joiMiddleware.joiBodyMiddleware(joiSchemas.userUpdation, 'user'), verifiedMiddleware.accoutDeactivated(), updateUserComponent);
router.delete('/', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), deleteUserComponent);


// follow user
router.put('/:id/follow', passportMiddleware.jwtAuth, followUserComponent);
router.put('/follow/:id/accept', passportMiddleware.jwtAuth, acceptFollowRequestComponent);
router.put('/follow/:id/reject', passportMiddleware.jwtAuth, rejectFollowRequestComponent);


module.exports = router;