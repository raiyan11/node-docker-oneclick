const express = require('express');
const router = express.Router();
const passportMiddleware = require('../../middlewares/passport.middleware');
const joiMiddleware = require('../../middlewares/joi.middleware');
const joiSchemas = require('../../lib/utils/joi.schemas');
const verifiedMiddleware = require('../../middlewares/verified.middleware');
const verifyComponent = require('../../components/v1/auth/verify');
const resendComponent = require('../../components/v1/auth/resend');
const userNameComponent = require('../../components/v1/auth/username.available');

const signUpComponent = require('../../components/v1/auth/signup');
const loginComponent = require('../../components/v1/auth/login');
const changePasswordComponent = require('../../components/v1/auth/change.password');
const forgotPasswordComponent = require('../../components/v1/auth/forgot.password');
const resetPasswordComponent = require('../../components/v1/auth/reset.password');
const refreshTokenComponent = require('../../components/v1/auth/refresh.token');

router.post('/signup', joiMiddleware.joiBodyMiddleware(joiSchemas.signUp, 'user'), signUpComponent);
router.post('/login', joiMiddleware.joiBodyMiddleware(joiSchemas.login, 'user'), loginComponent);

router.get('/username/:name', userNameComponent);
router.put('/verify/:id',passportMiddleware.jwtAuth,joiMiddleware.joiBodyMiddleware(joiSchemas.verifyCode, 'user'), verifiedMiddleware.accoutDeactivated(), verifyComponent);
router.put('/resend/:id', verifiedMiddleware.accoutDeactivated(), resendComponent);
router.put('/changepassword', passportMiddleware.jwtAuth,verifiedMiddleware.accoutDeactivated(), joiMiddleware.joiBodyMiddleware(joiSchemas.changePassword, 'user'), verifiedMiddleware.accountVerified(), changePasswordComponent);
router.post('/forgotpassword', joiMiddleware.joiBodyMiddleware(joiSchemas.forgotPassword, 'user'), forgotPasswordComponent);
router.put('/resetpassword', joiMiddleware.joiBodyMiddleware(joiSchemas.resetPassword, 'user'), resetPasswordComponent);
router.post('/refreshtoken', refreshTokenComponent);

module.exports = router;