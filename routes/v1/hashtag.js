const express = require('express');
const router = express.Router();
const passportMiddleware = require('../../middlewares/passport.middleware');


const getHashtagsComponent = require('../../components/v1/hashtags/get.hashtags');

router.get('', passportMiddleware.optionalJwtAuth, getHashtagsComponent);



module.exports = router;