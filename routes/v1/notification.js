const express = require('express');
const router = express.Router();
const passportMiddleware = require('../../middlewares/passport.middleware');
const verifiedMiddleware = require('../../middlewares/verified.middleware');

const getNotificationComponent = require('../../components/v1/notifications/get.notifications');
const markAsReadComponent = require('../../components/v1/notifications/mark.read');
const unReadNotificationCount = require('../../components/v1/notifications/get.notification.count');

router.get('/', passportMiddleware.jwtAuth, getNotificationComponent);
router.put('/:id/read', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), markAsReadComponent);
router.get('/unread', passportMiddleware.jwtAuth, unReadNotificationCount);

module.exports = router;
