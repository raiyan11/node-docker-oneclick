let express = require('express');
const configRouter = require('./config');
const presignRouter = require('./presign');
const authRouter = require('./auth');
const userRouter = require('./user');
const postRouter = require('./post');
const hashtagRouter = require('./hashtag')
const adminRouter = require('./admin');
const notificationRouter = require('./notification');

module.exports = {
    authRouter,
    configRouter,
    presignRouter,
    userRouter,
    postRouter,
    adminRouter,
    hashtagRouter,
    notificationRouter
};

