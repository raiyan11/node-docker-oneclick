const express = require('express');
const router = express.Router();
const joiMiddleware = require('../../middlewares/joi.middleware');
const joiSchemas = require('../../lib/utils/joi.schemas');
const userRoleMiddleWare = require('../../middlewares/user.role.middleware');
const constants = require('../../config/constants');
const passportMiddleWare = require('../../middlewares/passport.middleware');


const contactUsComponent = require('../../components/v1/config/contact.us')
const showComponent = require('../../components/v1/config/show');
const awskeyComponent = require('../../components/v1/config/awskey');
const imageSizeComponent = require('../../components/v1/config/test');

router.post('/size',imageSizeComponent);
router.get('/awskey',awskeyComponent);
router.get('/',showComponent);
router.post('/contactus', joiMiddleware.joiBodyMiddleware(joiSchemas.contactUs), contactUsComponent);

module.exports = router;
