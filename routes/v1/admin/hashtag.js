
//joiMiddleware.joiBodyMiddleware(joiSchemas.verifyCode), 
const express = require('express');
const router = express.Router();
const joiMiddleware = require('../../../middlewares/joi.middleware');
const joiSchemas = require('../../../lib/utils/joi.schemas');
const userRoleMiddleWare = require('../../../middlewares/user.role.middleware');
const constants = require('../../../config/constants');
const verifiedMiddleware = require('../../../middlewares/verified.middleware');
const passportMiddleware = require('../../../middlewares/passport.middleware');

const hashtagFetchComponent = require('../../../components/v1/admin/hashtag/fetch');
const hashtagCreateComponent = require('../../../components/v1/admin/hashtag/create');
const hashtagApproveComponent = require('../../../components/v1/admin/hashtag/approve.hashtag');
const hashtagRejectComponent = require('../../../components/v1/admin/hashtag/reject.hashtag');
const hashtagUpdateComponent = require('../../../components/v1/admin/hashtag/update');


router.get('/',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),hashtagFetchComponent);
router.post('/',passportMiddleware.jwtAuth,joiMiddleware.joiBodyMiddleware(joiSchemas.hashtagCreate,'hashtag'),userRoleMiddleWare.canAccess([constants.userRole.admin]),hashtagCreateComponent); 
router.put('/:id',passportMiddleware.jwtAuth,joiMiddleware.joiBodyMiddleware(joiSchemas.hashtagUpdate,'hashtag'),userRoleMiddleWare.canAccess([constants.userRole.admin]),hashtagUpdateComponent); 
router.put('/:id/approve',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),hashtagApproveComponent);
router.put('/:id/reject',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),hashtagRejectComponent);


module.exports = router;
