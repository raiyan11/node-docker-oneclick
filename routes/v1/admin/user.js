
//joiMiddleware.joiBodyMiddleware(joiSchemas.verifyCode), 
const express = require('express');
const router = express.Router();
const joiMiddleware = require('../../../middlewares/joi.middleware');
const joiSchemas = require('../../../lib/utils/joi.schemas');
const userRoleMiddleWare = require('../../../middlewares/user.role.middleware');
const constants = require('../../../config/constants');
const verifiedMiddleware = require('../../../middlewares/verified.middleware');
const passportMiddleware = require('../../../middlewares/passport.middleware');

const userFetchComponent = require('../../../components/v1/admin/user/fetch');
const userUpdateComponent = require('../../../components/v1/admin/user/update.approval');
const userFetchOneComponent = require('../../../components/v1/admin/user/fetch.one');
const deactivateUpdateComponent = require('../../../components/v1/admin/user/deactivate.user');
const getUserDetailsComponent = require('../../../components/v1/admin/user/user.details');

router.get('/',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),userFetchComponent);
router.get('/:id',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),userFetchOneComponent);
router.put('/:id/approve',passportMiddleware.jwtAuth,joiMiddleware.joiBodyMiddleware(joiSchemas.userApproved,'user'),userRoleMiddleWare.canAccess([constants.userRole.admin]),userUpdateComponent);
router.put('/:id/deactivate',userRoleMiddleWare.canAccess([constants.userRole.admin]),deactivateUpdateComponent);
router.get('/:id',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),getUserDetailsComponent);


module.exports = router;
