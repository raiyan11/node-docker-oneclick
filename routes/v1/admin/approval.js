
//joiMiddleware.joiBodyMiddleware(joiSchemas.verifyCode), 
const express = require('express');
const router = express.Router();
const joiMiddleware = require('../../../middlewares/joi.middleware');
const joiSchemas = require('../../../lib/utils/joi.schemas');
const userRoleMiddleWare = require('../../../middlewares/user.role.middleware');
const constants = require('../../../config/constants');
const verifiedMiddleware = require('../../../middlewares/verified.middleware');
const passportMiddleware = require('../../../middlewares/passport.middleware');

const fetchApprovalComponent = require('../../../components/v1/admin/approval/fetch');
const acceptApprovalComponent = require('../../../components/v1/admin/approval/approval.accept');
const rejectApprovalComponent = require('../../../components/v1/admin/approval/approval.reject');
const createCodeComponent = require('../../../components/v1/admin/approval/create.approval.code');
const listCodesComponent = require('../../../components/v1/admin/approval/list.codes');

router.get('/',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),fetchApprovalComponent);
router.put('/:id/accept',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),acceptApprovalComponent); 
router.put('/:id/reject',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),rejectApprovalComponent);
router.post('/code',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),createCodeComponent);
router.get('/code',passportMiddleware.jwtAuth,userRoleMiddleWare.canAccess([constants.userRole.admin]),listCodesComponent);

module.exports = router;
