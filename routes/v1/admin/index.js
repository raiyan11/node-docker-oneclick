let express = require('express');

const hashtagRouter = require('./hashtag');
const userRouter = require('./user');
const approvalRouter = require('./approval');

module.exports = {
    hashtagRouter,
    userRouter,
    approvalRouter
};
