const express = require('express');
const router = express.Router();
const passportMiddleware = require('../../middlewares/passport.middleware');
const joiMiddleware = require('../../middlewares/joi.middleware');
const joiSchemas = require('../../lib/utils/joi.schemas');
const verifiedMiddleware = require('../../middlewares/verified.middleware');

const createPostComponent = require('../../components/v1/post/create.post');
const updatePostComponent = require('../../components/v1/post/update.post');
const rePostComponent = require('../../components/v1/post/repost.post');
const deletePostComponent = require('../../components/v1/post/delete.post');
const likePostComponent = require('../../components/v1/post/like.post');
const unlikePostComponent = require('../../components/v1/post/unlike.post');
const getLikedPostsComponent = require('../../components/v1/post/get.liked');
const getMyPostComponent = require('../../components/v1/post/get.my.posts');
const reportPostComponent = require('../../components/v1/post/report.post');
const activatePostComponent = require('../../components/v1/post/activate.post');
const deactivatePostComponent = require('../../components/v1/post/deactivate.post');
const getOnePostComponent = require('../../components/v1/post/get.post');
const getAllPostComponent = require('../../components/v1/post/list');
const getAllHasNewComponent = require('../../components/v1/post/list.new');
const searchAllPostComponent = require('../../components/v1/post/search');
const searchAllPostHasNewComponent = require('../../components/v1/post/search.new');
const hidePostComponent = require('../../components/v1/post/post.hide');
const unhidePostComponent = require('../../components/v1/post/post.unhide');
const removePostComponent = require('../../components/v1/post/remove.post');
const getOtherUserPostsComponent = require('../../components/v1/post/get.other.posts');
const likeUserPostComponent = require('../../components/v1/post/get.liked.user.post');

const postHashtagsUnregisteredUserComponent = require('../../components/v1/post/posts.asper.hashtags');


router.post('/', passportMiddleware.jwtAuth, joiMiddleware.joiBodyMiddleware(joiSchemas.postCreate, 'post'), verifiedMiddleware.accoutDeactivated(), createPostComponent);
router.put('/hashtag', joiMiddleware.joiBodyMiddleware(joiSchemas.postHashtagPreferences, 'hashtags'), postHashtagsUnregisteredUserComponent);
router.put('/:id', passportMiddleware.jwtAuth, joiMiddleware.joiBodyMiddleware(joiSchemas.postUpdate, 'post'), verifiedMiddleware.accoutDeactivated(), updatePostComponent);
router.put('/:id/repost', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), rePostComponent);
router.delete('/:id', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), deletePostComponent);
router.put('/:id/hide', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), hidePostComponent);
router.put('/:id/unhide', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), unhidePostComponent);
router.put('/:id/like', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), likePostComponent);
router.get('/hasnew', passportMiddleware.optionalJwtAuth,joiMiddleware.joiQueryMiddleware(joiSchemas.hasNew), getAllHasNewComponent);
router.get('/:id/liked/users', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), likeUserPostComponent);
router.put('/:id/unlike', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), unlikePostComponent);
router.get('/liked', passportMiddleware.jwtAuth, getLikedPostsComponent);
router.get('/self', passportMiddleware.jwtAuth, getMyPostComponent);
router.get('/user/:id', passportMiddleware.optionalJwtAuth, getOtherUserPostsComponent);

router.put('/:id/report', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), reportPostComponent);
router.put('/:id/activate', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), activatePostComponent);
router.put('/:id/deactivate', passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(), deactivatePostComponent);
router.get('/search/hasnew', passportMiddleware.optionalJwtAuth,joiMiddleware.joiQueryMiddleware(joiSchemas.hasNew), searchAllPostHasNewComponent);
router.get('/search', passportMiddleware.optionalJwtAuth, searchAllPostComponent);
router.get('/:id', passportMiddleware.optionalJwtAuth, getOnePostComponent);
router.get('/', passportMiddleware.optionalJwtAuth, getAllPostComponent);
router.put('/:id/remove',passportMiddleware.jwtAuth, verifiedMiddleware.accoutDeactivated(),removePostComponent);


module.exports = router;