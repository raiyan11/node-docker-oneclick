const faker = require('faker');
const moment = require('moment');
const bcrypt = require('bcrypt');
const _ = require('lodash');
var randomatic = require("randomatic")
const { bcryptConfig, constants } = require('../config');

exports.seed = function (knex) {
  if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging') {

    const password = bcrypt.hashSync('Bitcot@123', bcryptConfig.hashRound);

    return knex('users').max('id as maxId').first().then(function (userRes) {
      console.log('userRes')
      console.log(userRes)
      let userId = userRes.maxId + 1;
      console.log(userId)
      return knex('posts').select('id').then(function (postRes) {

        //Total counts
        const usersTotalCount = userId + 50
        let postsCurrentCount = postRes.length

        //All posts id
        const allPostIds = postRes.map(item => item.id)

        const newPostIds = []
        _.range(0, 500).forEach(index => {
          newPostIds.push(randomatic('a0', 16)
          )
        })
        allPostIds.push(...newPostIds)

        console.log('allPostIds', allPostIds)

        return knex('attachments').max('id as maxId').first().then(function (attachmentRes) {
          console.log('attachmentRes')
          console.log(attachmentRes)
          let attachmentId = attachmentRes.maxId + 1;
          console.log(attachmentId)


          return knex('hashtags_posts').max('id as maxId').first().then(function (hashtagPostRes) {
            console.log('hashtagPostRes')
            console.log(hashtagPostRes)
            let hashtagPostId = hashtagPostRes.maxId + 1;
            console.log(hashtagPostId)

            return knex('hashtags_users').max('id as maxId').first().then(function (hashtagUserRes) {
              console.log('hashtagUserRes')
              console.log(hashtagUserRes)
              let hashtagUserId = hashtagUserRes.maxId + 1;
              console.log(hashtagUserId)


              return knex('follows').max('id as maxId').first().then(function (followsRes) {
                console.log('followsRes')
                console.log(followsRes)
                let followId = followsRes.maxId + 1;
                console.log(followId)


                return knex('blocks').max('id as maxId').first().then(function (blocksRes) {
                  console.log('blocksRes')
                  console.log(blocksRes)
                  let blockId = blocksRes.maxId + 1;
                  console.log(blockId)


                  return knex('hides').max('id as maxId').first().then(function (hidesRes) {
                    console.log('hidesRes')
                    console.log(hidesRes)
                    let hideId = hidesRes.maxId + 1;
                    console.log(hideId)

                    return knex('notifications').max('id as maxId').first().then(function (notificationsRes) {
                      console.log('notificationsRes')
                      console.log(notificationsRes)
                      let notificationId = notificationsRes.maxId + 1;
                      console.log(notificationId)


                      return knex('likes').max('id as maxId').first().then(function (likesRes) {
                        console.log('likesRes')
                        console.log(likesRes)
                        let likeId = likesRes.maxId + 1;
                        console.log(likeId)

                        return knex('activities').max('id as maxId').first().then(function (activitiesRes) {
                          console.log('activitiesRes')
                          console.log(activitiesRes)
                          let activityId = activitiesRes.maxId + 1;
                          console.log(activityId)

                          return knex('hashtags').where({ active_status: constants.activeStatus.active }).then(function (hashtagsResponse) {
                            console.log('hashtagsResponse')
                            console.log(hashtagsResponse)
                            // hashtags 
                            const testHastags = hashtagsResponse.map(item => item.name)

                            // Create users records
                            const userRecords = [];
                            // Create usersApproval records
                            const usersApprovalRecords = [];
                            // Create post records
                            const postRecords = [];
                            // Create activity records
                            const activityRecords = [];
                            // Create attachments records
                            const attachmentsRecords = [];
                            // Create hashtagspost records
                            const hashtagsPostRecords = [];
                            // Create hashtagsUser records
                            const hashtagsUserRecords = [];
                            // Create Follow records
                            const followUserRecords = [];
                            // Create Block records
                            const blockRecords = [];
                            // Create Hide records
                            const hideRecords = [];
                            // Create Notification records
                            const notificationRecords = [];
                            // Create Notification records
                            const likesRecords = [];

                            // profile Images
                            const profileImages = [
                              'uploads/user/7979/avatar1.jpeg',
                              'uploads/user/7979/avatar11.png',
                              'uploads/user/7979/avatar22.png',
                              'uploads/user/7979/avatar33.png',
                              'uploads/user/7979/avatar44.png',
                              'uploads/user/7979/avatar55.png',
                              'uploads/user/7979/avatar66.png',
                              'uploads/user/7979/avatar77.jpeg',
                              'uploads/user/7979/avatar88.jpg',
                              'uploads/user/7979/avatar99.png',
                              'uploads/user/7979/avatar10.png'];


                            // post Images
                            const postImages = [
                              'uploads/user/7979/post1.jpg',
                              'uploads/user/7979/post2.jpg',
                              'uploads/user/7979/post3.jpg',
                              'uploads/user/7979/post4.jpg',
                              'uploads/user/7979/post5.jpg',
                              'uploads/user/7979/post6.png',
                              'uploads/user/7979/post7.jpg',
                              'uploads/user/7979/post8.png',
                              'uploads/user/7979/post9.jpg',
                              'uploads/user/7979/post10.jpg',
                              'uploads/user/7979/post11.png',
                              'uploads/user/7979/post12.jpg',
                              'uploads/user/7979/post13.jpeg',
                              'uploads/user/7979/post14.jpeg',
                              'uploads/user/7979/post15.jpeg',
                              'uploads/user/7979/post16.png',
                              'uploads/user/7979/post17.png',
                              'uploads/user/7979/post18.jpeg',
                              'uploads/user/7979/post19.png',
                              'uploads/user/7979/post20.png'
                           ];
           const headerImages = ['uploads/user/2390/localhost_1601974652431.png','uploads/user/7073/localhost_1601974876103.png',
          'uploads/user/2016/localhost_1601974948184.png','uploads/user/9020/localhost_1601975056855.png','uploads/user/5533/localhost_1601975121412.png','uploads/user/1562/localhost_1601975185201.png',
         'uploads/user/9759/localhost_1601975240924.png','uploads/user/4430/localhost_1601975304176.png','uploads/user/8360/localhost_1601975373984.png','uploads/user/4521/localhost_1601975428141.png'];

                            for (let i = 0; i < 50; i++) {
                              const otp = Math.floor(1000 + Math.random() * 9000);
                              const userName = faker.internet.userName()
                              const user = {
                                id: userId.valueOf(),
                                user_name: userName,
                                first_name: faker.name.firstName(),
                                last_name: faker.name.lastName(),
                                email: faker.internet.email(),
                                password: password,
                                photo_url: profileImages[Math.floor(Math.random() * profileImages.length)],
                                header_image: headerImages[Math.floor(Math.random() * headerImages.length)],
                                bio: faker.lorem.sentence(),
                                verified: faker.random.boolean(),
                                verification_token: otp,
                                role: 'user',
                                privacy_settings: faker.random.boolean() ? 'public' : 'private',
                                active_status: 'active',
                                show_followers: faker.random.boolean(),
                                show_following: faker.random.boolean(),
                                created_at: moment(),
                                updated_at: moment(),
                                width: 230,
                                height:219,
                                header_width:1024,
                                header_height:512,
                                reset_password_token: faker.random.uuid(),
                                reset_password_sent_at: moment()
                              };
                              userRecords.push(user);

                              const approvalStatus = faker.random.boolean() ? 'accepted' : 'denied'
                              // user Approval
                              const userApproval = {
                                id: userId.valueOf(),
                                user_id: userId.valueOf(),
                                link: faker.internet.url(),
                                about: faker.lorem.sentence(),
                                status: approvalStatus,
                                active_status: 'active',
                                created_at: moment(),
                                updated_at: moment(),
                                own_content: true
                              };
                              usersApprovalRecords.push(userApproval);

                              if (approvalStatus == 'accepted') {
                                const notificationObj = {
                                  id: notificationId.valueOf(),
                                  entity_id: userId.valueOf(),
                                  entity_type: 'Approval',
                                  title: 'Approval request accepted',
                                  content: "You've been approved. You can post now",
                                  type: 'posting_approved',
                                  user_id: userId.valueOf(),
                                  created_at: moment().add(i * 100),
                                  updated_at: moment().add(i * 100),
                                }
                                notificationRecords.push(notificationObj)
                                notificationId++
                              }


                              //Follows adding
                              const followCount = Math.floor(Math.random() * (20 - 5)) + 5
                              for (let b = 0; b < followCount; b++) {

                                const followRecord = {
                                  id: followId.valueOf(),
                                  followee_id: Math.floor(Math.random() * (usersTotalCount - 1) + 1),
                                  follower_id: userId.valueOf(),
                                  status: 'accepted',
                                  active_status: 'active',
                                  created_at: moment(),
                                  updated_at: moment(),
                                };

                                const notificationObj = {
                                  id: notificationId,
                                  entity_id: followId.valueOf(),
                                  entity_type: 'Follow',
                                  title: 'Follow request accepted',
                                  content: "User has accepted your follow request",
                                  type: 'accepted_follow',
                                  user_id: userId.valueOf(),
                                  created_at: moment().add(i * 100 + b * 10),
                                  updated_at: moment().add(i * 100 + b * 10),
                                }
                                notificationRecords.push(notificationObj)
                                notificationId++

                                followUserRecords.push(followRecord)
                                followId++
                              }

                              //Blocks adding
                              const blockCount = Math.floor(Math.random() * 3)
                              for (let d = 0; d < blockCount; d++) {

                                const blockRecord = {
                                  id: blockId.valueOf(),
                                  user_id: userId.valueOf(),
                                  blocked_user_id: Math.floor(Math.random() * (usersTotalCount.valueOf() - 1) + 1),
                                  active_status: 'active',
                                  created_at: moment(),
                                  updated_at: moment(),
                                };

                                blockRecords.push(blockRecord)
                                blockId++
                              }

                              //User hashtags adding
                              for (let c = 1; c < 3; c++) {
                                var randomHashtag = testHastags[Math.floor(Math.random() * testHastags.length)];
                                const hashtagsUser = {
                                  id: hashtagUserId,
                                  show: faker.random.boolean(),
                                  user_id: userId.valueOf(),
                                  hashtag_name: randomHashtag,
                                  active_status: 'active',
                                  created_at: moment(),
                                  updated_at: moment(),
                                };

                                hashtagsUserRecords.push(hashtagsUser)
                                hashtagUserId++
                              }

                              for (let j = 0; j < 10; j++) {

                                const postId = allPostIds[postsCurrentCount];
                                // create posts
                                let randomHashtags = [];
                                // hashtagspost
                                const hashtagCount = Math.floor(Math.random() * 5)
                                for (let z = 0; z < hashtagCount; z++) {
                                  const randomHashtag = testHastags[Math.floor(Math.random() * testHastags.length)]
                                  randomHashtags.push(randomHashtag)

                                  const hashtagsPost = {
                                    id: hashtagPostId,
                                    post_id: postId,
                                    hashtag_name: randomHashtag,
                                    active_status: 'active',
                                    created_at: moment(),
                                    updated_at: moment(),
                                  };

                                  hashtagsPostRecords.push(hashtagsPost)
                                  hashtagPostId++
                                }

                                const postTime = moment().add(i * 10 + j)

                                const posts = {
                                  id: postId,
                                  //title: faker.name.title(),
                                  description: faker.name.title(),
                                  user_id: userId.valueOf(),
                                  likable: true,
                                  type: faker.random.boolean() ? 'text' : 'image',
                                  active_status: 'active',
                                  user_name: userName,
                                  hashtags: randomHashtags.join(','),
                                  created_at: postTime,
                                  updated_at: postTime,
                                };



                                //likes adding
                                const likesCount = Math.floor(Math.random() * 8)
                                for (let e = 0; e < likesCount; e++) {
                                  const otherUserId = Math.floor(Math.random() * (usersTotalCount.valueOf() - 1) + 1)
                                  const likeRecord = {
                                    id: likeId.valueOf(),
                                    user_id: otherUserId.valueOf(),
                                    post_id: postId,
                                    active_status: 'active',
                                    created_at: moment(),
                                    updated_at: moment(),
                                  };
                                  likesRecords.push(likeRecord)
                                  likeId++
                                  const notificationObj = {
                                    id: notificationId,
                                    entity_id: postId.valueOf(),
                                    entity_type: 'Post',
                                    title: 'Your post is liked',
                                    content: "User liked your post",
                                    type: 'liked_post',
                                    user_id: otherUserId.valueOf(),
                                    created_at: moment().add(i * 100 + j * 10 + e),
                                    updated_at: moment().add(i * 100 + j * 10 + e),
                                  }
                                  notificationRecords.push(notificationObj)
                                  notificationId++

                                }

                                // activity
                                const activity = {
                                  id: activityId.valueOf(),
                                  post_id: postId,
                                  likes_count: likesCount,
                                  active_status: 'active',
                                  created_at: moment(),
                                  updated_at: moment(),
                                };
                                // attachments
                                const attachments = {
                                  id: attachmentId.valueOf(),
                                  entity_type: 'Post',
                                  entity_id: postId,
                                  url: postImages[Math.floor(Math.random() * postImages.length)],
                                  active_status: 'active',
                                  created_at: moment(),
                                  updated_at: moment(),
                                  width: 1600,
                                  height: 1063,
                                };


                                // hides
                                const hideCount = Math.floor(Math.random() * 3)
                                for (let f = 0; f < hideCount; f++) {

                                  const hidePost = {
                                    id: hideId,
                                    user_id: userId.valueOf(),
                                    post_id: allPostIds[Math.floor(Math.random() * postImages.length)],
                                    active_status: 'active',
                                    created_at: moment(),
                                    updated_at: moment(),
                                  };

                                  hideRecords.push(hidePost)
                                  hideId++
                                }

                                attachmentsRecords.push(attachments);
                                activityRecords.push(activity);
                                postRecords.push(posts);
                                attachmentId++;
                                activityId++;
                                postsCurrentCount = postsCurrentCount + 1
                              }
                              userId++;
                            }
                            return knex('users').insert(userRecords).then(function () {
                              return knex('approvals').insert(usersApprovalRecords).then(function () {
                                return knex('posts').insert(postRecords).then(function () {
                                  return knex('activities').insert(activityRecords).then(function () {
                                    return knex('attachments').insert(attachmentsRecords).then(function () {
                                      return knex('hashtags_posts').insert(hashtagsPostRecords).then(function () {
                                        return knex('hashtags_users').insert(hashtagsUserRecords).then(function () {
                                          return knex('follows').insert(followUserRecords).then(function () {
                                            return knex('blocks').insert(blockRecords).then(function () {
                                              return knex('hides').insert(hideRecords).then(function () {
                                                return knex('notifications').insert(notificationRecords).then(function () {
                                                  return knex('likes').insert(likesRecords).then(function () {
                                                    return knex.raw(`ALTER SEQUENCE users_id_seq RESTART WITH ${userId}`).then(function () {
                                                      return knex.raw(`ALTER SEQUENCE approvals_id_seq RESTART WITH ${userId}`).then(function () {
                                                        return knex.raw(`ALTER SEQUENCE activities_id_seq RESTART WITH ${activityId}`).then(function () {
                                                          return knex.raw(`ALTER SEQUENCE attachments_id_seq RESTART WITH ${attachmentId}`).then(function () {
                                                            return knex.raw(`ALTER SEQUENCE hashtags_posts_id_seq RESTART WITH ${hashtagPostId}`).then(function () {
                                                              return knex.raw(`ALTER SEQUENCE hashtags_users_id_seq RESTART WITH ${hashtagUserId}`).then(function () {
                                                                return knex.raw(`ALTER SEQUENCE follows_id_seq RESTART WITH ${followId}`).then(function () {
                                                                  return knex.raw(`ALTER SEQUENCE blocks_id_seq RESTART WITH ${blockId}`).then(function () {
                                                                    return knex.raw(`ALTER SEQUENCE hides_id_seq RESTART WITH ${hideId}`).then(function () {
                                                                      return knex.raw(`ALTER SEQUENCE notifications_id_seq RESTART WITH ${notificationId}`).then(function () {
                                                                        return knex.raw(`ALTER SEQUENCE likes_id_seq RESTART WITH ${likeId}`).then(function () {
                                                                        });
                                                                      });
                                                                    });
                                                                  });
                                                                });
                                                              });
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  } else {
    return null;
  }

};
