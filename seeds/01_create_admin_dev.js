const moment = require('moment');
const bcrypt = require('bcrypt');
const { bcryptConfig } = require('../config');

exports.seed = function (knex) {
    return knex('users').where({ email: 'dev+admin@bitcot.com' }).select().then(function (rows) {
        console.log('rows')
        console.log(rows)
        if (rows.length === 0) {
            const password = 'Bitcot@123';
            return knex('users').insert([
                {
                    id: 1,
                    user_name: 'Dev Admin',
                    email: 'dev+admin@bitcot.com',
                    password: bcrypt.hashSync(password, bcryptConfig.hashRound),
                    role: 'admin',
                    verified: 'true',
                    privacy_settings: 'public',
                    created_at: moment(),
                    updated_at: moment()
                }]).then(function () {

                    knex('approvals').insert([
                        {
                            id: 1,
                            user_id: 1,
                            status: 'accepted',
                            active_status: 'active',
                            created_at: moment(),
                            updated_at: moment(),
                            own_content: true
                        }]).then(function () {
                            return knex.raw(`ALTER SEQUENCE users_id_seq RESTART WITH 2`).then(function () {
                                return knex.raw(`ALTER SEQUENCE approvals_id_seq RESTART WITH 2`)
                            })
                        });
                })
        } else {
            return null;
        }
    })
};
