const moment = require('moment');

exports.seed = function (knex) {
  let id = 1
  return knex('hashtags').count().then(function (res) {
    console.log('res')
    console.log(res)
    if (res && res.length > 0 && res[0].count == 0) {

      return knex('users').where({ email: 'dev+admin@bitcot.com' }).first().then(function (row) {
        console.log('row')
        console.log(row)
        let created_by = 1
        if (row.id) {
          created_by = row.id
        }
        const testHastags = ["#love", "instagood", "#photooftheday", "#beautiful", "#fashion", "#tbt", "#happy", "#cute", "#followme", "#like4like", "#follow", "#me", "#picoftheday", "#selfie", "#instadaily"]

        const hashtags = []

        testHastags.forEach(testHashtag => {
          const hashtag = {
            id,
            name: testHashtag,
            approved: true,
            created_at: moment(),
            updated_at: moment(),
            created_by
          }
          hashtags.push(hashtag)
          id++
        })
        console.log('hashtags', hashtags)

        return knex('hashtags').insert(hashtags).then(function () {
          return knex.raw(`ALTER SEQUENCE hashtags_id_seq RESTART WITH ${id}`)
        })

      })
    } else {
      return null
    }
  });
};
