const AWS = require('aws-sdk');
var fs = require('fs');
const _ = require('lodash');
var size = require('s3-image-size');
var awsObj = {};
if (process.env.NODE_ENV === 'development') {
    awsObj.accessKeyId = process.env.AWS_ACCESS_KEY,
        awsObj.secretAccessKey = process.env.AWS_SECRET_KEY,
        awsObj.region = "us-west-2"
}
else if (process.env.NODE_ENV === 'staging') {
    awsObj.accessKeyId = 'AKIAZLYNCOSQXBZTB7BJ',
        awsObj.secretAccessKey = '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',
        awsObj.region = 'us-west-2'
}

AWS.config.update(awsObj);
const generateUrl = (path, size = {}) => {
    return new Promise((resolve, reject) => {
        try {
            const options = {
                bucket: global_Vault_key.bucket,
                key: path,
                edits: size,
            };

            const objJsonStr = JSON.stringify(options);
            const objJsonB64 = Buffer.from(objJsonStr).toString('base64');
            const imageUrl = global_Vault_key.cloudfront + '/' + objJsonB64;
            return resolve(imageUrl);
        } catch (e) {
            return reject(new Error('Something went wrong.'));
        }
    });
};
module.exports = {
    presignarryOfUrl: async (userId, extData) => {
        try {
            const s3Urls = [];
            var s3 = new AWS.S3({ signatureVersion: 'v4' });
            const obtainSignedUrls = params =>
                new Promise(resolve =>
                    s3.getSignedUrl('putObject', params, (err, data) => (err ? resolve(false) : resolve(data)))
                );


            for (var i = 0; i < extData.length; i++) {
                console.log(extData[i]);
                const fileStoragePath = `${'uploads/user'}/${userId}/${global_Vault_key.domain}_${Date.now().toString()}${extData[i]}`;
                console.log(fileStoragePath);
                var params =
                {
                    Bucket: global_Vault_key.bucket,
                    Key: fileStoragePath,
                    ACL: 'public-read'
                };

                const signedUrL = await obtainSignedUrls(params);
                if (signedUrL) {
                    s3Urls.push({
                        signedUrL: signedUrL,
                        publicUrl: `${global_Vault_key.bucket + fileStoragePath}`,
                        publicDbUrl: fileStoragePath
                    });
                }
            }
            //console.log(s3Urls);
            return s3Urls;
        } catch (error) {
            return error;
        }
    },
    deleteFile: async (fileStoragePath) => {
        return new Promise((resolve, reject) => {
            try {
                const bucketInstance = new AWS.S3();
                const params = {
                    Bucket: global_Vault_key.bucket,
                    Key: fileStoragePath
                };
                bucketInstance.deleteObject(params, function (err, data) {
                    if (data) {
                        return resolve('File deleted successfully');
                    } else {
                        return resolve(false);
                    }
                });
            } catch (error) {
                return reject(error);
            }
        }
        );

    },
    s3GetSize: async (fileStoragePath, skip = true) => {
        return new Promise(async (resolve, reject) => {

            console.log('s3GetSize')
            const bucketInstance = new AWS.S3({region : 'us-west-2'});
            if (skip)
                return resolve({ height: 1000, width: 1000 });
            else {
                //TODO - below is failing without throwing error. Check. 
                console.log('bucket', global_Vault_key.bucket)
                console.log('fileStoragePath', fileStoragePath)
                size(bucketInstance, global_Vault_key.bucket, fileStoragePath, function
                    (err, dimensions, bytesRead) {

                    if (err) {
                        console.log('err', err)
                        return resolve({ height: 1000, width: 1000 });
                    }
                    else {
                        console.log('dimensions', dimensions)
                        return resolve(dimensions);
                    }
                });
            }
        }
        );
    },
    generateVersionNormalUrl: async (url) => {
        if (url && url.length > 0)
            return { original: url, small: url, medium: url };
        else
            return {};
    },
    generateVersionUrls: async (url, height, width) => {
        const path = (url.split('.com'))[1].substring(1);
        //TODO - Below code takes long time. Check it
        //   let imageSize= await getS3filesize(path)

        if (!path) {
            return {};
        } else {

            let mediumDevisor = 2
            let smallDevisor = 3

            if(width>2000 || height>2000){
                mediumDevisor = 3
                smallDevisor = 4
            }

            return {
                original: `${global_Vault_key.public_cloudfront_url}/${path}`,
                medium: await generateUrl(path, { 'resize': { 'width': Math.round(width / mediumDevisor), 'height': Math.round(height / mediumDevisor) }, 'normalize': true, 'sharpen': true }, { extension: path.split('.')[1] }),
                small: await generateUrl(path, { 'resize': { 'width': Math.round(width / smallDevisor), 'height': Math.round(height / smallDevisor) }, 'normalize': true, 'sharpen': true }, { extension: path.split('.')[1] }),
                webp: await generateUrl(path, { 'resize': { 'width': Math.round(width / mediumDevisor), 'height': Math.round(height / mediumDevisor) }, 'normalize': true, 'sharpen': true }, { extension: 'webp' })

                // original: await generateUrl(path, {'resize': { 'width': Math.floor(width),'height':Math.floor(height) },'normalize':true,'sharpen':true}),
                // medium: await generateUrl(path, {'normalize':true,'sharpen':true,'resize': { 'width': Math.floor(width/2), 'height':Math.floor(height/2) }}),
                // small: await generateUrl(path, {'normalize':true,'sharpen':true, 'resize': { 'width':  Math.floor(width/3), 'height':  Math.floor(height/3)} })
            };
        }
    },
    uploadBase64Image: async (qrId, base64Image) => {
        return new Promise((resolve, reject) => {
            buf = Buffer.from(base64Image.replace(/^data:image\/\w+;base64,/, ""), 'base64')
            var data = {
                Key: qrId,
                Body: buf,
                ContentEncoding: 'base64',
                ContentType: 'image/png'
            };
            var s3Bucket = new AWS.S3({ params: { Bucket: global_Vault_key.bucket } });

            s3Bucket.putObject(data, function (err, data) {
                if (err) {
                    console.log(err);
                    console.log('Error uploading data: ', data);
                    return reject(err)
                } else {
                    console.log('succesfully uploaded the image!');
                    console.log(data)
                    return resolve(data)
                }
            });
        }
        );
    }
};