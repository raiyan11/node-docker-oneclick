const jwt = require('jsonwebtoken');
//const { jwtConfig } = require('../../config');

 function  generate (user) {
  return jwt.sign(user, global_Vault_key.JwtsecretKey, {
    audience: global_Vault_key.domain,
    issuer: global_Vault_key.domain,
    expiresIn: '7d'
  });
}

function decodeSubscriptionToken(token){
  return jwt.decode(token);
}

function generateRefreshToken(user) {
  return jwt.sign(user, global_Vault_key.JwtsecretKey, {
    audience:  global_Vault_key.domain,
    issuer:  global_Vault_key.domain,
    expiresIn: '7d'
  });
}

function verifyToken(jwtToken) {
  return jwt.verify(jwtToken, global_Vault_key.JwtsecretKey, {
    audience:  global_Vault_key.domain,
    issuer:  global_Vault_key.domain,
    maxAge: '7d'
  });
}

function impersonatedUserToken(payload) {
  return jwt.sign(payload,global_Vault_key.JwtsecretKey, {
    expiresIn: '15m',
    audience: global_Vault_key.domain,
    issuer: global_Vault_key.domain
  });
}
const TokenType = {
  ID_TOKEN: 'idToken',
  REFRESH_TOKEN: 'refreshToken'
};
module.exports = { generate, generateRefreshToken, verifyToken, TokenType, impersonatedUserToken, decodeSubscriptionToken };
