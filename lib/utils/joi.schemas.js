const Joi = require('@hapi/joi');

const address = Joi.object({
    formatted_address: Joi.string(),
    city: Joi.string(),
    state: Joi.string(),
    zip: Joi.string(),
    street_address: Joi.string(),
    country: Joi.string(),
    latitude: Joi.number(),
    longitude: Joi.number(),
});

//Auth validations

module.exports.signUp = Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().required(),
    user_name: Joi.string().required(),
    photo_url: Joi.string(),
    bio: Joi.string(),
    first_name: Joi.string(),
    last_name: Joi.string(),
    session_info: Joi.object(),
    width: Joi.when('photo_url', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    }),
    height: Joi.when('photo_url', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    })
});
module.exports.userApproval =

    Joi.alternatives().try(
        Joi.object().keys({
            link: Joi.string(),
            photo_paths: Joi.array().items(Joi.object({
                url: Joi.string().required(),
                width: Joi.string().required(), height: Joi.string().required()
            })),
            about: Joi.string().required(),
            own_content: Joi.boolean()
        }),
        Joi.object().keys({
            link: Joi.string().required(),
            photo_paths: Joi.array().items(Joi.object({
                url: Joi.string().required(),
                width: Joi.string().required(), height: Joi.string().required()
            })),
            about: Joi.string().required(),
            own_content: Joi.boolean()
        })
    );

module.exports.userUpdation = Joi.object({
    bio: Joi.string(),
    first_name: Joi.string(),
    user_name: Joi.string(),
    last_name: Joi.string(),
    photo_url: Joi.string(),
    width: Joi.when('photo_url', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    }),
    height: Joi.when('photo_url', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    }),
    header_image: Joi.string(),
    header_width: Joi.when('header_image', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    }),
    header_height: Joi.when('header_image', {
        is: Joi.exist(),
        then: Joi.string().required(),
        otherwise: Joi.optional()
    }),
    show_followers: Joi.boolean(),
    show_following: Joi.boolean()
});
module.exports.postCreate = Joi.object({
    title: Joi.string(),
    description: Joi.string(),
    link: Joi.string(),
    type: Joi.string().valid('text', 'image'),
    photo_paths: Joi.array().items(Joi.object({
        url: Joi.string().required(),
        width: Joi.string().required(), height: Joi.string().required()
    })),
    likable: Joi.boolean()
});
module.exports.postUpdate = Joi.object({
    title: Joi.string(),
    description: Joi.string(),
    link: Joi.string(),
    type: Joi.string().valid('text', 'image'),
    photo_paths: Joi.array().items(Joi.object({
        url: Joi.string().required(),
        width: Joi.string().required(), height: Joi.string().required()
    })),
    likable: Joi.boolean(),
    remove_attachments: Joi.array().items(Joi.number().integer())
});

module.exports.login = Joi.object({
    user: Joi.string().required(),
    password: Joi.string().required(),
    session_info: Joi.object()
});

module.exports.changePassword = Joi.object({
    current_password: Joi.string().required(),
    password: Joi.string().required(),
    password_confirmation: Joi.string().required()
});
module.exports.verifyCode = Joi.object({
    code: Joi.string().required()
});
module.exports.resetPassword = Joi.object({
    reset_password_token: Joi.string().required(),
    password: Joi.string().required(),
    password_confirmation: Joi.string().required()
});
module.exports.userPolicy = Joi.object({
    privacy_settings: Joi.string().valid('public', 'private', 'registered'),
});
module.exports.forgotPassword = Joi.object({
    email: Joi.string().required()
});
module.exports.userHashtagSuggesh = Joi.object({
    name: Joi.string().required()
});

module.exports.hashtagCreate = Joi.object({
    name: Joi.string().required()
});

module.exports.hashtagUpdate = Joi.object({
    name: Joi.string()
});
module.exports.hashtagUpdate = Joi.object({
    name: Joi.string(),
    approved: Joi.boolean()
});
module.exports.userApproved = Joi.object({
    account_approved: Joi.boolean().required()
});

module.exports.userHashtagPreferences = Joi.object({
    show: Joi.array().items(Joi.string()),
    hide: Joi.array().items(Joi.string()),
    remove: Joi.array().items(Joi.string())
});

module.exports.hasNew = Joi.object({
    firstItemCreatedAt: Joi.string().required()
});


module.exports.postHashtagPreferences = Joi.object({
    show: Joi.array().items(Joi.string()),
    hide: Joi.array().items(Joi.string()),
});

module.exports.contactUs = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required(),
    description: Joi.string().required()
});