

module.exports = (async function() {
  const ssm = new (require('aws-sdk/clients/ssm'))();
  let dbData = await ssm.getParameters({
   Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbhostname_ks`,
`/lips/${process.env.NODE_ENV_VAULT}/dbpassword_ks`,
`/lips/${process.env.NODE_ENV_VAULT}/dbuser_ks`,
`/lips/${process.env.NODE_ENV_VAULT}/URL`,
`/lips/${process.env.NODE_ENV_VAULT}/WEBSITE_USER_URL`]
}).promise();

let secretKey = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/SENDGRID_API_KEY_SAFE`],'WithDecryption': true}).promise();
        
let awsData = await ssm.getParameters({
 Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_BUCKET`,
`/lips/${process.env.NODE_ENV_VAULT}/AWS_CLOUDFRONT_URL`,
`/lips/${process.env.NODE_ENV_VAULT}/AWS_PUBLIC_URL`,
`/lips/${process.env.NODE_ENV_VAULT}/APP_DOMAIN`,
`/lips/${process.env.NODE_ENV_VAULT}/PORT`,
`/lips/${process.env.NODE_ENV_VAULT}/AWS_REGION`]
}).promise();
let jwtandEmailData = await ssm.getParameters({
Names:[`/lips/${process.env.NODE_ENV_VAULT}/JWT_SECRET_KEY`,
`/lips/${process.env.NODE_ENV_VAULT}/SENDGRID_FROM`,
`/lips/${process.env.NODE_ENV_VAULT}/IOS_MIN_VERSION`,
`/lips/${process.env.NODE_ENV_VAULT}/IOS_VERSION`,
`/lips/${process.env.NODE_ENV_VAULT}/ANDROID_MIN_VERSION`,
`/lips/${process.env.NODE_ENV_VAULT}/ANDROID_VERSION`]
}).promise();

let originalCloudfront= await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_ORIGINAL_CLOUDFRONT_URL`]}).promise();

let dbPort= await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbport_ks`]}).promise();
let dbDatabase= await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbname_ks`]}).promise();

let response= [...dbData.Parameters,...awsData.Parameters,...jwtandEmailData.Parameters, ...secretKey.Parameters, ...originalCloudfront.Parameters, ...dbPort.Parameters,...dbDatabase.Parameters]
let responseData=response.map(Vault=>Vault.Value)

global_Vault_key.webUrl=responseData[0];
if(!global_Vault_key.webUrl){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}//WEBSITE_USER_URL`]}).promise();
      global_Vault_key.webUrl=Data.Parameters[0].Value ;
  }
global_Vault_key.url=responseData[1];
if(!global_Vault_key.url){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/URL`]}).promise();
     global_Vault_key.url=Data.Parameters[0].Value ;
  }
global_Vault_key.host=responseData[2];
if(!global_Vault_key.host){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbhostname_ks`]}).promise();
     global_Vault_key.host=Data.Parameters[0].Value ;
  }
global_Vault_key.password=responseData[3];
if(!global_Vault_key.password){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbpassword_ks`]}).promise();
     global_Vault_key.password=Data.Parameters[0].Value ;
  }
global_Vault_key.user=responseData[4];
if(!global_Vault_key.user){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbuser_ks`]}).promise();
     global_Vault_key.user=Data.Parameters[0].Value ;
  }

global_Vault_key.domain=responseData[5];
if(!global_Vault_key.domain){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/APP_DOMAIN`]}).promise();
     global_Vault_key.domain=Data.Parameters[0].Value ;
  }
global_Vault_key.bucket=responseData[6];
if(!global_Vault_key.bucket){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_BUCKET`]}).promise();
     global_Vault_key.bucket=Data.Parameters[0].Value ;
  }
global_Vault_key.cloudfront=responseData[7];
if(!global_Vault_key.cloudfront){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_CLOUDFRONT_URL`]}).promise();
     global_Vault_key.cloudfront=Data.Parameters[0].Value ;
  }
global_Vault_key.publicUrl=responseData[8];
if(!global_Vault_key.publicUrl){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_PUBLIC_URL`]}).promise();
     global_Vault_key.publicUrl=Data.Parameters[0].Value ;
  }
global_Vault_key.region=responseData[9];
if(!global_Vault_key.region){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_REGION`]}).promise();
     global_Vault_key.region=Data.Parameters[0].Value ;
  }
global_Vault_key.port=responseData[10];
if(!global_Vault_key.region){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_REGION`]}).promise();
     global_Vault_key.region=Data.Parameters[0].Value ;
  }
global_Vault_key.androidMin=responseData[11];
if(!global_Vault_key.androidMin){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/ANDROID_MIN_VERSION`]}).promise();
     global_Vault_key.androidMin=Data.Parameters[0].Value ;
  }
global_Vault_key.androidMax=responseData[12];
if(!global_Vault_key.androidMax){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/ANDROID_VERSION`]}).promise();
     global_Vault_key.androidMax=Data.Parameters[0].Value ;
  }
global_Vault_key.iosMin=responseData[13];
if(!global_Vault_key.iosMin){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/IOS_MIN_VERSION`]}).promise();
     global_Vault_key.iosMin=Data.Parameters[0].Value ;
  }
global_Vault_key.iosMax=responseData[14];
if(!global_Vault_key.iosMax){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/IOS_VERSION`]}).promise();
     global_Vault_key.iosMax=Data.Parameters[0].Value ;
  }
global_Vault_key.JwtsecretKey=responseData[15];
if(!global_Vault_key.JwtsecretKey){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/JWT_SECRET_KEY`]}).promise();
     global_Vault_key.JwtsecretKey=Data.Parameters[0].Value ;
  }
global_Vault_key.SendgridFrom=responseData[16];
if(!global_Vault_key.SendgridFrom){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/SENDGRID_FROM`]}).promise();
     global_Vault_key.SendgridFrom=Data.Parameters[0].Value ;
  }
global_Vault_key.SendgridKey=responseData[17];
if(!global_Vault_key.SendgridKey){
  let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/SENDGRID_API_KEY_SAFE`],'WithDecryption': true}).promise();
     global_Vault_key.SendgridKey=Data.Parameters[0].Value ;
  }
  global_Vault_key.public_cloudfront_url=responseData[18];
  if(!global_Vault_key.public_cloudfront_url){
    let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/AWS_ORIGINAL_CLOUDFRONT_URL`]}).promise();
       global_Vault_key.public_cloudfront_url=Data.Parameters[0].Value ;
    }
    global_Vault_key.dbport=responseData[19];
    if(!global_Vault_key.dbport){
      let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbport_ks`]}).promise();
         global_Vault_key.dbport=Data.Parameters[0].Value ;
      }
      global_Vault_key.database=responseData[20];
      if(!global_Vault_key.database){
        let Data = await ssm.getParameters({Names:[`/lips/${process.env.NODE_ENV_VAULT}/dbname_ks`]}).promise();
           global_Vault_key.database=Data.Parameters[0].Value ;
        }



  console.log(global_Vault_key.dbport);
})();




