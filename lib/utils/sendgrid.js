'use strict';
const Email = require('email-templates');
const template = new Email({
  juiceResources: {
    preserveImportant: true,
    webResources: {
      images: false
    }
  }
});

const path = require('path');
const sgMail = require('@sendgrid/mail');

class SendGridEmail {
	
	static send(subject, templatePath, payload, toAddresses = [], ccAddresses = []) {
		sgMail.setApiKey(global_Vault_key.SendgridKey);
       return new Promise(async (resolve, reject) => {
			const html = await template.render(path.join(__dirname, '../../views', templatePath), payload);
			sgMail
				.send({
					to: toAddresses,
					cc: ccAddresses,
					from: global_Vault_key.SendgridFrom,
					subject,
					html
				})
				.then(r => resolve(true))
				.catch(e => resolve(e));
		});
	}
}

module.exports = SendGridEmail;
