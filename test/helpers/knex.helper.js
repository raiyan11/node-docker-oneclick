const _ = require('lodash');
const passportMiddleWare = require('../../middlewares/passport.middleware');
const faker = require('faker');

const knex = require('knex')({
    client: 'pg',
    connection: {
        host: 'localhost',
        user: 'postgres',
        password: '',
        database: 'database_inharmony',
        charset: 'utf8'
    },
    debug: false
});

module.exports = {

    getAccessTokenAndUser: async () => {
        const sql_query = `SELECT * FROM users WHERE verified = true AND role = 'user' ORDER BY RANDOM() LIMIT 1`;
        const queryResponse = await knex.raw(sql_query);
        const row = queryResponse.rows[0];
        const user = _.omit(row, ['password', 'reset_password_token', 'created_at', 'updated_at', 'reset_password_sent_at', 'verification_token']);
        const {token} = passportMiddleWare.generateSignUpToken(user);
        return {token, user};
    },
    getAccessTokenAdmin: async () => {
        const sql_query = `SELECT * FROM users WHERE verified = true AND role = 'admin' ORDER BY RANDOM() LIMIT 1`;
        const queryResponse = await knex.raw(sql_query);
        const row = queryResponse.rows[0];
        const adminuser = _.omit(row, ['password', 'reset_password_token', 'created_at', 'updated_at', 'reset_password_sent_at', 'verification_token']);
        const {token} = passportMiddleWare.generateSignUpToken(adminuser);
        let admintoken =token;
        
        return {admintoken, adminuser};
    },
    getHarmony: async () => {
        const sql_query = `SELECT * FROM harmonies WHERE active_status='active' ORDER BY RANDOM() LIMIT 1`;
        const queryResponse = await knex.raw(sql_query);
        const row = queryResponse.rows[0];
        return row;
    },

};