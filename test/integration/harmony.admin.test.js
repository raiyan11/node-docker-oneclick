const request = require('supertest');
const app = require('../../app');
const faker = require('faker');
const {knexHelper} = require('../helpers');
let profileId;
let userId;
let harmonyValue;
const {constants} = require('../../config');
let accessToken;
let unverifiedAccountAccessToken;
const _ = require('lodash');


beforeAll(async () => {

    //Unverified token
   // unverifiedAccountAccessToken = await knexHelper.getAccessTokenBasedOnVerified(false);
    const {token, user} = await knexHelper.getAccessTokenAndUser();
    let  harmonyData= await knexHelper.getHarmony();
    let {admintoken, adminuser} = await knexHelper.getAccessTokenAdmin();
    accessToken = admintoken;
    userId=user.id;
    harmonyValue=harmonyData.id

console.log('abbas_________---')
console.log(harmonyValue)
});

describe('harmony test', () => {


    /*----------------------  Harmony tests ----------------------*/
 
  //Invalid Token
    it('Create Harmony Invalid Token', done => {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        request(app)
            .post('/v1/admin/harmony')
            .set('Authorization', `Bearer ${unverifiedAccountAccessToken}`)
            .send({
                harmony: {
                    name: firstName,
                    trainer: lastName,
                    duration:'11',
                    description:faker.lorem.paragraph(),
                    thumbnail_url:'uploads/user/2985/localhost_1598610922764.jpg',
                    audio_url:'uploads/user/2985/localhost_1598610922764.jpg',
                    config:[
                        {
                            name:faker.name.firstName(),
                            of_type:'type'
                        },   {
                            name:faker.name.firstName(),
                            of_type:'category'
                        },
                        {
                            name:faker.name.firstName(),
                            of_type:'category'
                        }
                    ]
                }
            })
            .then(response => {
                expect(response.statusCode).toBe(401);
                expect(response.body.success).toBe(false);
                done();
            }).catch(error => {
            console.log('error - ');
            console.log(error);
        });
    });
    it('Create Harmony Valid Token', done => {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        request(app)
            .post('/v1/admin/harmony')
            .set('Authorization', `Bearer ${accessToken}`)
            .send({
                harmony: {
                    name: firstName,
                    trainer: lastName,
                    duration:'11',
                    description:faker.lorem.paragraph(),
                    thumbnail_url:'uploads/user/2985/localhost_1598610922764.jpg',
                    audio_url:'uploads/user/2985/localhost_1598610922764.jpg',
                    config:[
                        {
                            name:faker.name.firstName(),
                            of_type:'type'
                        },   {
                            name:faker.name.firstName(),
                            of_type:'category'
                        },
                        {
                            name:faker.name.firstName(),
                            of_type:'level'
                        }
                    ]
                }
            })
            .then(response => {
                console.log(response.body);
                expect(response.statusCode).toBe(200);
                expect(response.body.success).toBe(true);
                done();
            }).catch(error => {
            console.log('error - ');
            console.log(error);
        });
    });
//Update Harmony
    it('Update Harmony success', done => {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        request(app)
            .put(`/v1/admin/harmony/${harmonyValue}`)
            .set('Authorization', `Bearer ${accessToken}`)
            .send({
                 harmony: {
                        name: firstName,
                        trainer: lastName,
                        duration:'11',
                        description:faker.lorem.paragraph(),
                        thumbnail_url:'uploads/user/2985/localhost_1598610922764.jpg',
                        audio_url:'uploads/user/2985/localhost_1598610922764.jpg',
                        config:[
                            {
                                name:faker.name.firstName(),
                                of_type:'type'
                            },   {
                                name:faker.name.firstName(),
                                of_type:'category'
                            },
                            {
                                name:faker.name.firstName(),
                                of_type:'level'
                            }
                        ]
                    }
            })
            .then(response => {
                console.log(response.body);
                expect(response.statusCode).toBe(200);
                expect(response.body.success).toBe(true);
                expect(response.body.harmony).toBeDefined();
                done();
            }).catch(error => {
            console.log('error - ');
            console.log(error);
        });
    });
//Failed with extra items in body
    it('Update Harmony failed with extra items in body', done => {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        request(app)
            .put(`/v1/admin/harmony/${harmonyValue}`)
            .set('Authorization', `Bearer ${accessToken}`)
            .send({
                harmony: {
                    first_name: firstName,
                    last_name: lastName,
                    email: faker.internet.email()
                }
            })
            .then(response => {
                expect(response.body.code).toBe(400);
                expect(response.body.success).toBe(false);
                done();
            }).catch(error => {
            console.log('error - ');
            console.log(error);
        });
    });

 // Failed with unverified 
    it('Update Harmony failed if unverified', done => {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        request(app)
            .put(`/v1/admin/harmony/${harmonyValue}`)
            .send({
                harmony: {
                    first_name: firstName,
                    last_name: lastName
                }
            })
            .then(response => {
                expect(response.body.message).toBe(constants.error.auth.noAuthToken);
                expect(response.body.success).toBe(false);
                done();
            }).catch(error => {
            console.log('error - ');
            console.log(error);
        });
    });
    //get User
 it('Get Haemony list Success', done => {
        request(app)
            .get(`/v1/admin/harmony`)
            .set('Authorization', `Bearer ${accessToken}`)
            .then(response => {
                expect(response.statusCode).toBe(200);
                expect(response.body.success).toBe(true);
                expect(response.body.harmony).toBeDefined();
                done();
            }).catch(error => {
            console.log('error - ');
            console.log(error);
        });
    });
    // unAthorized
    it('Get Haemony unauthorized', done => {
        request(app)
            .get(`/v1/admin/harmony`)
            .then(response => {
                expect(response.body.message).toBe(constants.error.auth.noAuthToken);
                expect(response.body.success).toBe(false);
                done();
            }).catch(error => {
            console.log('error - ');
            console.log(error);
        });
    });
//     //delete User unauthorized
//     it('Delete User unauthorized', done => {
//         request(app)
//             .delete(`/v1/user/${userId}`)
//             .then(response => {
//                 expect(response.body.message).toBe(constants.error.auth.noAuthToken);
//                 expect(response.body.success).toBe(false);
//                 done();
//             }).catch(error => {
//             console.log('error - ');
//             console.log(error);
//         });
//     });
//     // Delete User Success
//     it('Delete User Success', done => {
//         request(app)
//             .delete(`/v1/user/${userId}`)
//             .set('Authorization', `Bearer ${accessToken}`)
//             .then(response => {
//                 expect(response.statusCode).toBe(200);
//                 expect(response.body.success).toBe(true);
//                 expect(response.body.user).toBeDefined();
//                 done();
//             }).catch(error => {
//             console.log('error - ');
//             console.log(error);
//         });
//     });

    
});