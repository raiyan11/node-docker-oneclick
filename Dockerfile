FROM node:14-buster-slim
# ENV NODE_ENV=staging

WORKDIR /app

# install deps
COPY package.json /app

#importfroms3 
ADD https://kubernetes-mount-s3-raiyan.s3-us-west-2.amazonaws.com/lips-node.zip /app
RUN apt-get update && apt-get install unzip && unzip -o lips-node.zip   

RUN npm install
        
#run
EXPOSE 8080
CMD ["npm","run","start:staging"]
