const bookshelf = require('../config/bookshelf');

const Follow = bookshelf.Model.extend({
    tableName: "follows",
    hasTimestamps: true,
    autoIncrement: true,
    follower: function () {
        return this.belongsTo('User', 'follower_id');
    },
    followee: function () {
        return this.belongsTo('User', 'followee_id');
    },
    notifications() {
        return this.morphMany('Notification', 'entity', ['entity_type', 'entity_id'], 'Follow');
      }
})

module.exports = bookshelf.model("Follow", Follow);