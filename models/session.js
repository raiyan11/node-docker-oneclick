const bookshelf = require('../config/bookshelf');
const Session = bookshelf.Model.extend({
    tableName: "session",
    hasTimestamps: true,
    autoIncrement: true,
})

module.exports = bookshelf.model("Session", Session);