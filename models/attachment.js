const bookshelf = require('./../config/bookshelf');
const {awsUtil} = require('../lib/utils');
const _ = require('lodash');

const Attachment = bookshelf.Model.extend({
    tableName: 'attachments',
    hasTimestamps: true,
    autoIncrement: true,
    initialize: function () {

        this.on('saving', async function (model, attributes, options) {
          /* if (attributes.url && (!attributes.width || !attributes.height)){
                  let s3imagesize= await awsUtil.s3GetSize(attributes.url);
                   attributes.width =s3imagesize.width;
                   attributes.height =s3imagesize.height;
              }*/
          });

        this.on('fetched', async function (model, attributes, options) {
            //Fetching photo urls
            if (!attributes.url) {
                attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
            } else {
                const result = attributes.url.substr(0, 5);
                if (result === 'https')
                    attributes.photo_urls = await awsUtil.generateVersionNormalUrl(attributes.url);
                else {
                    attributes.photo_urls = await awsUtil.generateVersionUrls(global_Vault_key.publicUrl  + attributes.url,attributes.height,attributes.width);
                }
            }
        });
        this.on('fetched:collection', async function (model, attributes, options) {
            //Fetching photo urls
            if (model.models) {
                await asyncForEach(model.models, async innerModel => {
                    if (!innerModel.attributes.url) {
                        innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
                    } else {
                        const result = innerModel.attributes.url.substr(0, 5);
                        if (result === 'https')
                            innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl(innerModel.attributes.url);
                        else {
                            innerModel.attributes.photo_urls = await awsUtil.generateVersionUrls(global_Vault_key.publicUrl + innerModel.attributes.url,innerModel.attributes.height,innerModel.attributes.width);
                        }
                    }
                });
            }
        });
    },
    attachable() {
        return this.morphTo('attachable', ['entity_type', 'entity_id'], 'Post','Approve');
    }
});

Attachment.prototype.toJSON = function () {
    const that = this.attributes;
    return _.omit(that, ['sort', 'meta', 'created_at', 'updated_at', 'entity_type', 'entity_id']);
};

module.exports = bookshelf.model('Attachment', Attachment);