const bookshelf = require('../config/bookshelf');
require('./user');

const HashtagUser = bookshelf.Model.extend({
    tableName: "hashtags_users",
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
    }
})

module.exports = bookshelf.model("HashtagUser", HashtagUser);