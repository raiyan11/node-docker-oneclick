const bookshelf = require('../config/bookshelf');
const _ = require('lodash');
const HashtagPost = bookshelf.Model.extend({
    tableName: 'hashtags_posts',
    hasTimestamps: true,
    autoIncrement: true,
    posts: function() {
        return this.belongsTo('Post');
     }
    
});
module.exports = bookshelf.model('HashtagPost', HashtagPost);