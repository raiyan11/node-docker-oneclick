const bookshelf = require('./../config/bookshelf');
const _ = require('lodash');
const User = require('./user')

const Code = bookshelf.Model.extend({
    tableName: 'codes',
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
     }
});
module.exports = bookshelf.model('Code', Code);