const bookshelf = require('./../config/bookshelf');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const { bcryptConfig, constants } = require('../config');
const awsUtil = require("../lib/utils/aws.util");
const Follow = require("./follow")


const User = bookshelf.Model.extend({
    tableName: 'users',
    hasTimestamps: true,
    autoIncrement: true,

    initialize: function () {

        this.on('saving', async function (model, attributes, options) {

            if (attributes.password && this.hasChanged()) {
                attributes.password = bcrypt.hashSync(attributes.password, bcryptConfig.hashRound);
            }
            if (!attributes.verification_token) {
                let otp = Math.floor(1000 + Math.random() * 9000);
                attributes.verification_token = otp;
            }
            if(attributes.email){
                attributes.email = attributes.email.toLowerCase()
            }
            /*if (attributes.photo_url) {
                let s3imagesize = await awsUtil.s3GetSize(attributes.photo_url);
                attributes.width = s3imagesize.width;
                attributes.height = s3imagesize.height;
            }
            if (attributes.header_image) {
                let s3imagesize = await awsUtil.s3GetSize(attributes.header_image);
                attributes.header_width = s3imagesize.width;
                attributes.header_height = s3imagesize.height;
            }*/
        });
        this.on('fetched', async function (model, attributes, options) {

            //Relations helper
            if (model.relations) {
                model.attributes.approval_status = constants.approvalStatus.notSubmitted
                const obj = model.relations;
                //set initialy

                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        if (key == 'approval') {
                            model.attributes.approval_status = model.related(key).toJSON().status
                        } else if (key == 'followers')
                            model.attributes.followers_count = model.related(key).toJSON().length
                        else if (key == 'following')
                            model.attributes.following_count = model.related(key).toJSON().length
                        else if (key == 'followerSingle')
                            model.attributes.is_following = model.related(key).toJSON().length > 0
                        model.attributes[key] = model.related(key).toJSON();
                    }
                }
            }
            if (!model.attributes.header_image) {
                model.attributes.header_images = await awsUtil.generateVersionNormalUrl('');
            } else {
                const result = model.attributes.header_image.substr(0, 5);
                if (result === 'https')
                    model.attributes.header_images = await awsUtil.generateVersionNormalUrl(model.attributes.header_image);
                else
                    model.attributes.header_images = await awsUtil.generateVersionUrls(global_Vault_key.publicUrl + model.attributes.header_image, model.attributes.header_height, model.attributes.header_width);
            }
            if (!model.attributes.photo_url) {
                model.attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
            } else {
                const result = model.attributes.photo_url.substr(0, 5);
                if (result === 'https')
                    model.attributes.photo_urls = await awsUtil.generateVersionNormalUrl(model.attributes.photo_url);
                else {
                    model.attributes.photo_urls = await awsUtil.generateVersionUrls(global_Vault_key.publicUrl + model.attributes.photo_url, model.attributes.height, model.attributes.width);
                }
            }
        });
        this.on('fetched:collection', async function (model, attributes, options) {


            //Relations helper
            if (model.models) {

                model.models.forEach(innerModel => {
                    if (innerModel.relations) {
                        const obj = innerModel.relations;
                        innerModel.attributes.approval_status = constants.approvalStatus.notSubmitted

                        for (var key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                if (key == 'approval')
                                    innerModel.attributes.approval_status = innerModel.related(key).toJSON().status
                                else if (key == 'followers')
                                    innerModel.attributes.followers_count = innerModel.related(key).toJSON().length
                                else if (key == 'following')
                                    innerModel.attributes.following_count = innerModel.related(key).toJSON().length
                                else if (key == 'followerSingle')
                                    innerModel.attributes.is_following = innerModel.related(key).toJSON().length > 0
                                innerModel.attributes[key] = innerModel.related(key).toJSON();
                            }
                        }

                    }
                });
            }

            //Fetching image urls
            if (model.models) {
                await asyncForEach(model.models, async innerModel => {
                    if (!innerModel.attributes.header_image)
                        innerModel.attributes.header_images = await awsUtil.generateVersionNormalUrl('');
                    else {
                        const result = innerModel.attributes.header_image.substr(0, 5);
                        if (result === 'https')
                            innerModel.attributes.header_images = await awsUtil.generateVersionNormalUrl(innerModel.attributes.header_image);
                        else
                            innerModel.attributes.header_images = await awsUtil.generateVersionUrls(global_Vault_key.publicUrl + innerModel.attributes.header_image, innerModel.attributes.header_height, innerModel.attributes.header_width);
                    }


                    if (!innerModel.attributes.photo_url) {
                        innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl('');
                    } else {
                        const result = innerModel.attributes.photo_url.substr(0, 5);
                        if (result === 'https')
                            innerModel.attributes.photo_urls = await awsUtil.generateVersionNormalUrl(innerModel.attributes.photo_url);
                        else {
                            innerModel.attributes.photo_urls = await awsUtil.generateVersionUrls(global_Vault_key.publicUrl + innerModel.attributes.photo_url, innerModel.attributes.height, innerModel.attributes.width);
                        }
                    }
                });
            }
        });
    },
    followers: function () {
        return this.hasMany('Follow', 'followee_id');
    },
    following: function () {
        return this.hasMany('Follow', 'follower_id');
    },
    blocked: function () {
        return this.hasMany('Block');
    },
    approval() {
        return this.hasOne('Approval')
    },
    hides() {
        return this.hasMany('Hide')
    },
    reports() {
        return this.hasMany('Report')
    },
    blocked() {
        return this.hasMany('Block')
    },
    blockedMe() {
        return this.hasMany('Block', 'blocked_user_id')
    },
    hastagUsers() {
        return this.hasMany('HashtagUser')
    },
    followerSingle: function () {
        return this.hasMany('Follow', 'followee_id');
    }
});

User.prototype.toJSON = function () {
    const that = this.attributes;

    const ommit = ['password','followerSingle', 'reset_password_token', 'reset_password_sent_at', 'verification_token', 'height', 'width', 'header_width', 'header_height']

    if (that.approval && !that.approval.created_at)
        ommit.push('approval')
    if (that.followers && !that.followers.created_at)
        ommit.push('followers')
    if (that.following && !that.following.created_at)
        ommit.push('following')
    if (!that.created_at)
        ommit.push('approval_status')
    return _.omit(that, ommit);
};


module.exports = bookshelf.model('User', User);