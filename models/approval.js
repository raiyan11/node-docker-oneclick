const bookshelf = require('./../config/bookshelf');
const _ = require('lodash');
const User = require('./user')

const Approval = bookshelf.Model.extend({
    tableName: 'approvals',
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
     },
     attachments() {
        return this.morphMany('Attachment', 'attachable', ['entity_type', 'entity_id'], 'Approve');
   }
});
module.exports = bookshelf.model('Approval', Approval);