const bookshelf = require('./../config/bookshelf');
const _ = require('lodash');
const Post = require('./post')
const Activity = bookshelf.Model.extend({
    tableName: 'activities',
    hasTimestamps: true,
    autoIncrement: true,
    post: function() {
        return this.belongsTo('Post');
     }
    
});
module.exports = bookshelf.model('Activity', Activity);