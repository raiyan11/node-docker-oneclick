const bookshelf = require('./../config/bookshelf');
const _ = require('lodash');
var randomatic = require("randomatic")

const Post = bookshelf.Model.extend({
   tableName: 'posts',
   hasTimestamps: true,
   autoIncrement: true,

   initialize: function () {
      this.on('creating', async function (model, attributes, options) {
         if (!attributes.id)
            attributes.id = randomatic('a0', 16);
      });

      this.on('fetched', async function (model, attributes, options) {


         //Relations helper
         if (model.relations) {
            const obj = model.relations;
            for (var key in obj) {
               if (obj.hasOwnProperty(key)) {
                  if (key == 'is_liked') {
                     model.attributes['liked'] = model.related(key).toJSON().length == 1
                  } else if (key == 'activity')
                     model.attributes['likes_count'] = model.related(key).toJSON().likes_count
                  else if (key == 'reposts')
                     model.attributes['is_reposted'] = model.related(key).toJSON().length > 0
                  else
                     model.attributes[key] = model.related(key).toJSON();
               }
            }
         }
      });
      this.on('fetched:collection', async function (model, attributes, options) {

         console.log('fetching collection');

         //Relations helper
         if (model.models) {
            model.models.forEach(innerModel => {
               if (innerModel.relations) {
                  const obj = innerModel.relations;
                  for (var key in obj) {
                     if (obj.hasOwnProperty(key)) {
                        if (key == 'is_liked')
                           innerModel.attributes['liked'] = innerModel.related(key).toJSON().length == 1
                        else if (key == 'activity')
                           innerModel.attributes['likes_count'] = innerModel.related(key).toJSON().likes_count
                        else if (key == 'reposts')
                           innerModel.attributes['is_reposted'] = innerModel.related(key).toJSON().length > 0
                        else
                           innerModel.attributes[key] = innerModel.related(key).toJSON();
                     }
                  }
               }
            });
         }

      });
   },

   user: function () {
      return this.belongsTo('User', 'user_id');
   },
   attachments() {
      return this.morphMany('Attachment', 'attachable', ['entity_type', 'entity_id'], 'Post');
   },
   hashtagPosts() {
      return this.hasMany('HashtagPost')
   },
   is_liked: function () {
      return this.hasMany('Like')
   },
   activity: function () {
      return this.hasOne('Activity')
   },
   parent: function () {
      return this.belongsTo('Post', 'parent_id')
   },
   notifications() {
      return this.morphMany('Notification', 'entity', ['entity_type', 'entity_id'], 'Post');
   },
   reposts() {
      return this.hasMany('Post', 'parent_id')
   }
});

Post.prototype.toJSON = function () {
   const that = this.attributes;
   return _.omit(that, ['is_liked', 'activity', 'user_name', 'document', 'hashtags', 'reposts']);
};
module.exports = bookshelf.model('Post', Post);