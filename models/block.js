const bookshelf = require('../config/bookshelf');
require('./user');

const Block = bookshelf.Model.extend({
    tableName: "blocks",
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
    },
    blockedUser: function(){
        return this.belongsTo('User','blocked_user_id');
    }
})

module.exports = bookshelf.model("Block", Block);