const bookshelf = require('../config/bookshelf');
const _ = require('lodash');
const Like = bookshelf.Model.extend({
    tableName: 'likes',
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
     },
    post: function() {
        return this.belongsTo('Post');
     }
    
});
module.exports = bookshelf.model('Like', Like);