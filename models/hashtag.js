const bookshelf = require('../config/bookshelf');
const _ = require('lodash');
const HashTag = bookshelf.Model.extend({
    tableName: 'hashtags',
    hasTimestamps: true,
    autoIncrement: true,
});
module.exports = bookshelf.model('HashTag', HashTag);