const bookshelf = require('../config/bookshelf');
const _ = require('lodash');
const User = require('./user')

const Notification = bookshelf.Model.extend({
    tableName: 'notifications',
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
     },
     entity() {
        return this.morphTo('entity', ['entity_type', 'entity_id'], 'Follow','Post');
    }
});
module.exports = bookshelf.model('Notification', Notification);