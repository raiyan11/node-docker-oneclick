const bookshelf = require('../config/bookshelf');
const _ = require('lodash');
const Hide = bookshelf.Model.extend({
    tableName: 'hides',
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
     },
    posts: function() {
        return this.belongsTo('Post');
     }
    
});
module.exports = bookshelf.model('Hide', Hide);