const bookshelf = require('../config/bookshelf');
const _ = require('lodash');
const User = require('./user')
const Post = require('./post')
const Report = bookshelf.Model.extend({
    tableName: 'reports',
    hasTimestamps: true,
    autoIncrement: true,
    user: function() {
        return this.belongsTo('User');
     },
    posts: function() {
        return this.belongsTo('Post');
     }
    
});
module.exports = bookshelf.model('Report', Report);