'use strict';
const { ErrorHandler } = require('../lib/utils');
const Notification = require('../models/notification');

module.exports =  {

    createNotification: async (entity_id,entity_type,title,content,type,user_id) => {
        return new Promise(async (resolve, reject) => {
            const notificationObj = {
                entity_id: entity_id,
                entity_type: entity_type,
                title: title,
                content: content,
                type: type,
                user_id: user_id
            }
            console.log(notificationObj)
            const notification = await Notification.forge(notificationObj).save();
            console.log(notification);
            return resolve(notification);
        });
    }
}