const VaultKey = require('../lib/utils/vault.key');

if (process.env.NODE_ENV === 'development') {
  const knex = require('knex')({
    client: 'pg',
    connection: {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      charset: 'utf8'
    },
    debug: process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'staging'
  });

  module.exports.knex = knex;

}
else if (process.env.NODE_ENV === 'staging') {

  const knex = require('knex')({
    client: 'pg',
    connection: async () => {
      let loadKey = await VaultKey.envKey()
      return {
        host: global_Vault_key.host,
        user: global_Vault_key.user,
        password: global_Vault_key.password,
        database: global_Vault_key.database,
        port: global_Vault_key.dbport
      };
    }
  });
  module.exports.knex = knex;

}else if (process.env.NODE_ENV === 'production') {

  const knex = require('knex')({
    client: 'pg',
    connection: async () => {
      let loadKey = await VaultKey.envKey()
      return {
        host: global_Vault_key.host,
        user: global_Vault_key.user,
        password: global_Vault_key.password,
        database: global_Vault_key.database
      };
    }
  });
  module.exports.knex = knex;

}
