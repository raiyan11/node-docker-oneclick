'use strict';

const constants = Object.freeze({
    userRole: {
        admin: 'admin',
        user: 'user'
    },
    approvalStatus: {
        notSubmitted: 'not_submitted',
        requested: 'requested',
        accepted: 'accepted',
        denied: 'denied'
    },
    error: {
        auth: {
            HashtagAlreadyExists: 'Hashtag already exists',
            emailTaken: 'Email has already been taken',
            userNameTaken: 'UserName has already been taken.',
            phoneNumberTaken: 'Number has already been taken',
            codeMismatch: 'Verification code mismatch',
            invalidToken: 'Invalid Token',
            invalidCredentials: 'Invalid user credentials',
            invalidUser: 'Invalid user',
            userNotFound: 'User not found',
            sameUser: 'Cannot send request to self',
            userNotVerified: 'User not verified',
            userProfileNotFound: 'User Profile not found',
            deckNotFound: 'Deck Not Found',
            unauthorized: 'Unauthorized',
            noAuthToken: 'No auth token',
            profileNotFound: 'Profile not found',
            invalidAuthToken: 'Invalid auth token',
            deckServiceCheck: 'All ready DeckService Exit',
            passwordNotMatch: 'Confirm Password does not match.',
            passwordWrong: 'Current Password is wrong.',
            invalidPasswordToken: 'Invalid Password Token',
            invalidEmailToken: 'Invalid Email Token',
            emailNotVerified: 'Email not verified',
            harmonyNotfound: 'harmony not found',
            harmonyConfig: 'harmony Config Worng',
            accessDenied: 'Access Denied',
            userNotSame: 'Cannot signup with this user. Please check user id',
            userNameNotPresent: 'Username not sent',
            userNameNotAvailable: 'Username not available as it is already taken.',
            blockUser: 'You have not blocked the user',
            Followuser: 'You are not following the user',
            wrongFollowuser: 'Followuser user wrong..!',
            nothingToUpdate: 'Nothing to update',
            followNotFound: 'Follow request not found.',
            requestAlreadySent: 'you have already requested this User.',
            requestAlreadyAccepted: 'you have already accepted by this User.',
            wrongBlockUser: 'User to block not found!',
            userAlreadyBlocked: 'User already blocked',
            followNotFound: 'Follow request not found.',
            blockedByUser: 'You have been blocked by this user',
            approvalRequested: 'Approval has already been requested',
            approvalAccepted: 'Approval has already been accepted',
            approvalDenied: 'Approval has already been denied',
            approvalNotFound: 'User has not requested for approval',
            userDeactivated: 'User Is Deactivated. Please activate to perform this action',
            notificationNotFound: 'Notification not found',
            codeNotFound: 'Code Not Found.',
            codeAlreadySent: 'Code is already sent to this email',
            userIsNotPublic: 'Requested user is not public'
        },
    },

    post: {
        hashtagNotFound: 'Hashtag not found',
        hashtagsInvalid: 'One or more hashtag is invalid',
        hashtagNotFoundOrApproved: 'Hashtag not found or already approved',
        accountNotApproved: 'Your account is not approved',
        noHashTag: 'Please use atleast one hashtag',
        notFound: 'Post not Found',
        alreadyLikedByUser: 'Post already liked by this user',
        alreadyDislikedByUser: 'Post not liked by user',
        alreadyReportedByUser: 'Post already reported by this user',
        notBelongToUser: 'Post does not belong to user',
        duplicateRepost: 'Already reposted this post by this user',
        postNotUsers: 'Post does not belong to your user',
        cannotUpdateRepost: 'Cannot update a reposted post',
        privatePost: 'This post is not publicly available'
    },

    activeStatus: {
        active: 'active',
        inactive: 'inactive',
        deleted: 'deleted',
        hidden: 'hidden'
    },
    requestStatus: {
        requested: 'requested',
        accepted: 'accepted',
        denied: 'denied'
    },
    followStatus: {
        not_requested: 'not_requested',
        requested: 'requested',
        accepted: 'accepted',
        denied: 'denied'
    },
    privacySetting: {
        public:'public',
        private: 'private'
    },
    postType: {
        text: 'text',
        image: 'image',
        repost: 'repost'
    },
    userFeedFields: [
        'id', 'user_name', 'photo_url', 'height', 'width', 'header_width', 'header_height', 'header_image','privacy_settings'
    ],
    userFollowFields: [
        'id', 'user_name', 'photo_url', 'height', 'width', 'header_width', 'header_height', 'header_image', 'privacy_settings'
    ]


});

module.exports = constants;