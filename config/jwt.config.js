'use strict';
module.exports = {
  issuer:  process.env.APP_DOMAIN,
  audience: process.env.APP_DOMAIN,
  expiresIn: '48h',
  secretKey:'216a0123373da8753cd7354628195ea9f236a7bf5aac19278cb2a759db7873ddd6dcf7c68986a21073edb6ed648644397809b391f444ea426d7c5875da85c620'
};
