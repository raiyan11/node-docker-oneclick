const {constants} = require('../config');
const {ErrorHandler} = require('../lib/utils');

module.exports.canAccess = (roles) => {
    return (req, res, next) => {
        const userRole = req.user.role;
        console.log(`userRole - ${userRole}`);
        console.log(roles);
        if (roles.includes(userRole)) {
            next();
        } else
            res.serverError(401, ErrorHandler(new Error(constants.error.auth.accessDenied)));
    };
};