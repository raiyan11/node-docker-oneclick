const passport = require('passport');
let passportJwt = require('passport-jwt');

let LocalStratergy = require('passport-local').Strategy;
const CustomStrategy = require('passport-custom').Strategy;
let JWTStratergy = passportJwt.Strategy;
let ExtractJWT = passportJwt.ExtractJwt;
const { jwtConfig, constants } = require('../config');
const { jwtUtil, ErrorHandler, awsUtil, twilioUtil } = require('../lib/utils');
const bcrypt = require('bcrypt');


const User = require('../models/user');
//const UserSetting = require('../models/user_settings');

passport.use(
    new LocalStratergy(
        {
            usernameField: 'user[user]',
            passwordField: 'user[password]',
            passReqToCallback: true
        },
        async (req, email, password, done) => {
            // Login Stratergy here!
            try {
                let user = await User.query({
                    andWhere: { email: email.toLowerCase() },
                    orWhere: { user_name: email },
                }).where('active_status', '!=', constants.activeStatus.deleted)
                    .fetch({ require: false, withRelated: [{ 'approval': query => query.select('status', 'user_id')},{'followers': query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }).select('id','followee_id', 'status', 'active_status') }, { 'following': query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }).select('id','follower_id', 'status', 'active_status') }] });
                if (user) {
                    if (bcrypt.compareSync(password, user.attributes.password)) {
                        let userJson = user.toJSON();
                        const tokens = generateTokens(userJson);
                        return done(null, { user: userJson, ...tokens });
                    }
                }
                throw new Error(constants.error.auth.invalidCredentials);
            } catch (error) {
                return done(error, null);
            }
        }
    )
);

passport.use(
    new JWTStratergy(
        {
            ...jwtConfig,
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
            secretOrKey: jwtConfig.secretKey
        },
        async (jwtPayload, done) => {
            // extract information from payload
            // if user found `return done(null, user);` else `return done(error, null);`
            try {
                if (jwtPayload.type !== jwtUtil.TokenType.ID_TOKEN) {
                    throw new Error(constants.error.auth.invalidToken);
                }
                const user = await User.where({ id: jwtPayload.id }).fetch({ require: false });
                if (user) {
                    return done(null, user.toJSON());
                } else {
                    return done(constants.error.auth.invalidUser, null);
                }
            } catch (error) {
                return done(error, null);
            }
        }
    )
);

passport.use(
    'verifyRefreshToken',
    new CustomStrategy(async function (req, done) {
        if (req.headers['x-refreshtoken']) {
            const refreshToken = req.headers['x-refreshtoken'].toString();
            try {
                const decodedPayload = jwtUtil.verifyToken(refreshToken);
                if (decodedPayload.type !== jwtUtil.TokenType.REFRESH_TOKEN) {
                    throw new Error('Invalid token');
                }
                const user = await User.where({ id: decodedPayload.id, email: decodedPayload.email }).fetch({ require: false });
                let userJSON = user.toJSON();
                const tokens = generateTokens(userJSON);
                return done(null, { user, ...tokens });
            } catch (error) {
                return done(error, null);
            }
        }
        done('refresh token missing', null);
    })
);

/**
 * @description Generates idToken & refreshToken
 * @param {JSON Object} payload
 */
function generateTokens(payload) {
    const token = jwtUtil.generate({ ...payload, type: jwtUtil.TokenType.ID_TOKEN });
    const refresh_token = jwtUtil.generateRefreshToken({ ...payload, type: jwtUtil.TokenType.REFRESH_TOKEN });
    //   const twilio_token = twilioUtil.generateToken(payload.email);
    return { token, refresh_token };
}

module.exports.generateSignUpToken = function (userJson) {

    const tokens = generateTokens(userJson);
    return { user: userJson, ...tokens };
};

module.exports.jwtAuth = (req, res, next) =>
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
        if (err && err.name && err.name === 'TokenExpiredError') {
            if (err || info) return res.serverError(401, ErrorHandler(err));
        }
        if (info && info.name && info.name === 'TokenExpiredError') {
            if (err || info) return res.serverError(401, ErrorHandler(info));
        }
        if (err || info) return res.serverError(401, ErrorHandler(err || info));
        req.user = user;
        return next();
    })(req, res, next);

module.exports.optionalJwtAuth = (req, res, next) =>
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
        if (err && err.name && err.name === 'TokenExpiredError') {
            if (err || info) return res.serverError(401, ErrorHandler(err));
        }
        if (info && info.name && info.name === 'TokenExpiredError') {
            if (err || info) return res.serverError(401, ErrorHandler(info));
        }
        if (err && err.toString().startsWith('Error: No auth token')) {
            console.log('err', err)
            return next()
        }
        if (info && info.toString().startsWith('Error: No auth token')) {
            console.log('info', info.toString())
            return next()
        }
        if (err || info) return res.serverError(401, ErrorHandler(err || info));
        req.user = user;
        return next();
    })(req, res, next);


module.exports.guestAuth = (req, res, next) => {
    if (req.headers.authorization) {
        return this.jwtAuth(req, res, next)
    }
    return next();
}