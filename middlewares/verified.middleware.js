'use strict';
const { ErrorHandler } = require('../lib/utils');
const { constants } = require('../config');

module.exports.accountVerified = (schema, key) => {
    return (req, res, next) => {

        //Check if user present
        const user = req.user;
        if (!user) {
            res.serverError(400, ErrorHandler(new Error(constants.error.auth.userNotFound)));
        }
        //Check if user verified
       /* else if (!user.verified) {
            res.serverError(403, ErrorHandler(new Error(constants.error.auth.userNotVerified)));
        }*/ else
            next();
    };
};

module.exports.accoutDeactivated = (schema, key) => {
    return (req, res, next) => {
       const user = req.user;
        if (!user) {
            res.serverError(400, ErrorHandler(new Error(constants.error.auth.userNotFound)));
        }
        //Check if user is deactivated
        else if (user.active_status === constants.activeStatus.inactive) {
            res.serverError(403, ErrorHandler(new Error(constants.error.auth.userDeactivated)));
        } else
            next();
    };
}
