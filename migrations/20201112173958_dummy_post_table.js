exports.up = function(knex) {
    return knex.schema.createTable('dummy_post', t => {
        t.increments();
        t.string('email').unique().notNullable();
        t.string('code');
        t.dateTime('expires_at');
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['email'], 'index_codes_on_email_dummy');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('dummy_post');
};

