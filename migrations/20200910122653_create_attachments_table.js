exports.up = function (knex) {
    return knex.schema.createTable('attachments', t => {
        t.increments();
        t.string('entity_type');
        t.string('entity_id');
        t.string('url');
        t.string('width');
        t.string('height');
        t.string('sort');
        t.jsonb('meta').defaultTo({});
        t.enu('active_status', null, {
            useNative: true,
            existingType: true,
            enumName: 'active_status'
        }).defaultTo('active');
        t.timestamps();
        t.index(['entity_type', 'entity_id'], 'index_attachments_on_entity_type_and_entity_id');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('attachments');
};
