
exports.up = function(knex) {
    return knex.schema.createTable('activities', t => {
        t.increments();
        t.string('post_id').references('id').inTable('posts').notNullable();
        t.bigInteger('likes_count').defaultTo(0)
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['post_id'], 'index_activity_on_post_id');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('activities');
};
