exports.up = function(knex) {
    return knex.schema.createTable('hides', t => {
        t.increments();
        t.bigInteger('user_id').references('id').inTable('users').notNullable();
        t.string('post_id').references('id').inTable('posts').notNullable();
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['user_id'], 'index_hides__on_user_id_post_id');
     });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('hides');
};