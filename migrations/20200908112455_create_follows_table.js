exports.up = function(knex) {
    return knex.schema.createTable('follows', t => {
        t.increments();
        t.bigInteger('followee_id').references('id').inTable('users').notNullable();
        t.bigInteger('follower_id').references('id').inTable('users').notNullable();
        t.enu('status', ['requested','accepted','denied'], { useNative: false }).defaultTo('requested');
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['followee_id'], 'index_follow_on_followee_id');
        t.index (['follower_id'], 'index_follow_on_follower_id');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('follows');
};
