
exports.up = function (knex) {
    return knex.schema.table('follows', t => {
        t.dropColumn('block_status')
    });
};

exports.down = function (knex) {
    return knex.schema.table('follows', t => {
        t.boolean ('block_status').defaultTo(false);
    });
};
