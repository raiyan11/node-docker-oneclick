exports.up = function (knex) {

    return knex.schema.raw(`
    ALTER TABLE "notifications"
    DROP CONSTRAINT "notifications_type_check",
    ADD CONSTRAINT "notifications_type_check" 
    CHECK (type IN ('liked_post', 'requested_follow', 'accepted_follow', 'shared_your_post', 'posting_approved', 'started_follow'))
  `);
};

exports.down = function (knex) {
    return knex.schema.table('notifications', t => {
        t.enu('type', ['liked_post', 'requested_follow', 'accepted_follow', 'shared_your_post', 'posting_approved'], { useNative: false }).alter();
    });
};
