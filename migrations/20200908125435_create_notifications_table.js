exports.up = function(knex) {
    return knex.schema.createTable('notifications', t => {
        t.increments();
        t.bigInteger('user_id').references('id').inTable('users').notNullable();
        t.string('entity_id');
        t.string('entity_type');
        t.string('title');
        t.text('content');
        t.enu('type', ['liked_post', 'requested_follow', 'accepted_follow', 'shared_your_post', 'posting_approved'], { useNative: false }).notNullable();
        t.boolean('read').defaultTo(false);
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['user_id'], 'index_notifications_on_user_id');
        t.index (["entity_type", "entity_id"], "index_notifications_on_entity_type_and_entity_id");
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('hashtags_users');
};