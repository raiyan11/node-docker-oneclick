
exports.up = function(knex) {
    return knex.schema.createTable('hashtags', t => {
        t.increments();
        t.string('name').notNullable();
        t.bigInteger('created_by').references('id').inTable('users').notNullable();
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.boolean('approved').defaultTo(false)
        t.bigInteger('count').defaultTo(0);
        t.timestamps();
        t.index (['created_by'], 'index_hashtag_on_created_by');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('hashtags');
};
