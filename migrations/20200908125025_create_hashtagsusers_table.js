exports.up = function(knex) {
    return knex.schema.createTable('hashtags_users', t => {
        t.increments();
        t.bigInteger('user_id').references('id').inTable('users').notNullable();
        t.string('hashtag_name').notNullable();
        t.boolean('show');
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['user_id'], 'index_hastaguser_on_user_id');
        t.index (['hashtag_name'], 'index_hastaguser_on_hashtag_name');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('hashtags_users');
};