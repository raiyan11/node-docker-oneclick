exports.up = function(knex) {
    return knex.schema.createTable('session', t => {
       t.increments();
       t.string('`device_token`');
       t.string('app_version');
       t.string('os_version');
       t.string('device_manufacturer');
       t.string('device_model');
       t.dateTime('last_active').notNull();
       t.bigInteger('user_id').references('id').inTable('users').notNullable();
       t.enu('platform', ['ios', 'android', 'web'], { useNative: false }).defaultTo('web');
       t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
       t.timestamps();
   });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('session');
};