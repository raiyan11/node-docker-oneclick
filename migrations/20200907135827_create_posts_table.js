
exports.up = function (knex) {
    return knex.schema.createTable('posts', t => {
        t.string('id').primary().notNullable();
        t.string('title');
        t.text('description');
        t.bigInteger('user_id').references('id').inTable('users').notNullable();
        t.string('link');
        t.boolean('likable').defaultTo(true);
        t.enu('type', ['text', 'image', 'repost'], { useNative: false }).defaultTo('post');
        t.string('parent_id').references('id').inTable('posts');
        t.string('user_name');
        t.text('hashtags');
        t.specificType('document', 'tsvector');
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index(['user_id'], 'index_posts_on_user');
        t.index('document', null, 'gin');
    }).raw(`
    CREATE FUNCTION posts_tsvector_trigger() RETURNS trigger AS $$
        begin
          new.document :=
          setweight(to_tsvector(coalesce(new.description, '')), 'A')
          || setweight(to_tsvector(coalesce(new.user_name, '')), 'B')
          || setweight(to_tsvector(coalesce(new.hashtags, '')), 'C');
          return new;
        end
    $$ LANGUAGE plpgsql;`)
        .raw(`CREATE TRIGGER poststsvectorupdate BEFORE INSERT OR UPDATE
            ON posts FOR EACH ROW EXECUTE PROCEDURE posts_tsvector_trigger();
`)
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('posts').raw('DROP TRIGGER poststsvectorupdate ON services')
        .raw('DROP FUNCTION posts_tsvector_trigger');;
};
