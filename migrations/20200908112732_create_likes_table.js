exports.up = function(knex) {
    return knex.schema.createTable('likes', t => {
        t.increments();
        t.string('post_id').references('id').inTable('posts').notNullable();
        t.bigInteger('user_id').references('id').inTable('users').notNullable();
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['post_id', 'user_id'], 'index_likes_on_user_post_id');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('likes');
};
