exports.up = function(knex) {
    return knex.schema.createTable('hashtags_posts', t => {
        t.increments();
        t.string('post_id').references('id').inTable('posts').notNullable();
        t.string('hashtag_name').notNullable();
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['post_id'], 'index_hastagpost_on_post_id');
        t.index (['hashtag_name'], 'index_hastagpost_on_hashtag_name');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('hashtags_posts');
};
