exports.up = function(knex) {
    return knex.schema.createTable('blocks', t => {
        t.increments();
        t.bigInteger('user_id').references('id').inTable('users').notNullable();
        t.bigInteger('blocked_user_id').references('id').inTable('users').notNullable();
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['user_id', 'blocked_user_id'], 'index_blocks_on_user_blocked_id');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('blocks');
};
