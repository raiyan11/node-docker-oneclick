exports.up = function (knex) {
    return knex.schema.table('hashtags', t => {
        t.string('name').notNullable().unique().alter()
    });
};

exports.down = function (knex) {
    return knex.schema.table('hashtags', t => {
        t.string('name').notNullable().alter()
    });
};
