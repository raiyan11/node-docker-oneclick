
exports.up = function (knex) {
    return knex.schema.raw("CREATE TYPE active_status AS ENUM ( 'active', 'inactive', 'deleted', 'hidden')").createTable('users', t => {
        t.increments();
        t.string('user_name').unique();
        t.string('first_name');
        t.string('last_name');
        t.string('email').unique().notNullable();
        t.string('password');
        t.string('photo_url');
        t.string('width');
        t.string('height');
        t.string('header_image');
        t.string('header_width');
        t.string('header_height');
        t.text('bio')
        t.boolean('verified').defaultTo(false);
        t.string('verification_token');
        t.string('reset_password_token');
        t.dateTime('reset_password_sent_at');
        t.dateTime('terms_agreed_at');
        t.boolean ('show_followers').defaultTo(true);
        t.boolean ('show_following').defaultTo(true);
        t.enu('role', ['user', 'admin'], { useNative: false }).defaultTo('user');
        t.enu('privacy_settings', ['public', 'private', 'registered'], { useNative: false }).defaultTo('public');
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index(['email'], 'index_user_on_email');
    });
};

exports.down = function (knex) {
    return knex.schema.raw('DROP TYPE active_status').dropTable('users')
};
