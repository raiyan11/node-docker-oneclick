exports.up = function(knex) {
    return knex.schema.createTable('approvals', t => {
        t.increments();
        t.bigInteger('user_id').references('id').inTable('users').notNullable();
        t.string('link');
        t.text('about');
        t.enu('status', ['requested','accepted','denied'], { useNative: false }).defaultTo('requested');
        t.boolean('own_content').defaultTo(true);
        t.enu('active_status', null, { useNative: true, existingType: true, enumName: 'active_status' }).defaultTo('active');
        t.timestamps();
        t.index (['user_id'], 'index_approvals_on_user_id');
     });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('approvals');
};