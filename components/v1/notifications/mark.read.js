'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const User = require('../../../models/user');
const constants = require('../../../config/constants');
const Notification = require('../../../models/notification');

module.exports = async (req, res, next) => {
    try {
        const id = req.params.id;
        // get the notication of params id.
        const notification = await Notification.where(
            {
                id: id,
                user_id: req.user.id,
                active_status: constants.activeStatus.active
            })
            .fetch({ require: false, columns: ['id', 'user_id', 'active_status', 'updated_at'] });
        if (!notification)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.notificationNotFound)));
        console.log(notification);
        // get the notification befor the params id
        const notifications = await Notification.where('updated_at', '<=', notification.attributes.updated_at)
            .where({
                read: false,
                active_status: constants.activeStatus.active,
                user_id: req.user.id
            })
            .fetchAll({ require: false })

        // mark notifications to read.
        if (notifications.length > 0) {
            const notificationlist = []
            notifications.forEach(item => {
                const markReadObj = {
                    id: item.id,
                    read: true
                }
                notificationlist.push(markReadObj)
            })
            await Notification.collection(notificationlist).invokeThen('save');
        }
        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}