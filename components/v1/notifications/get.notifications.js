'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const User = require('../../../models/user');
const Post = require('../../../models/post');
const Follow = require('../../../models/follow');
const constants = require('../../../config/constants');
const Notification = require('../../../models/notification');
const bookshelf = require('../../../config/bookshelf');
const _ = require('lodash')
let page =1;
let limit =10;
module.exports = async (req, res, next) => {
    try {
        const userId = req.user.id;

        console.log('running notification db')
        let notificationsDb = await Notification.where({ 'user_id': userId, active_status: constants.activeStatus.active }).orderBy('created_at', 'desc').fetchAll({ require: false, columns: ['id', 'user_id', 'type', 'read','entity_id','entity_type'] });
        notificationsDb = notificationsDb.toJSON()
        // return res.success({ notificationsMerged:notificationsDb.length,notificationsDb})
    
        //Grouping unread notification
        console.log('running unread notification')
        let groupedUnreadNotification = []
        let likedPostIndex = -1;
        let groupedUnreadIndex = -1
        notificationsDb.every((notification, index) => {
            if (!notification.read) {
                if (notification.type == 'liked_post') {
                    if (likedPostIndex > -1) {
                        groupedUnreadNotification[likedPostIndex].count = groupedUnreadNotification[likedPostIndex].count + 1
                    } else {
                        likedPostIndex = index
                        notification.count = 1
                        groupedUnreadNotification.push(notification)
                    }
                } else {
                    groupedUnreadNotification.push(notification)
                }
                return false
            } else {
                groupedUnreadIndex = index
                return true
            }
        })

      // how ho check the rewrite code
        //Grouping read notification
        // console.log('running read notification')
        // if (groupedUnreadIndex > 0) {
        //     notificationsDb = notificationsDb.slice(groupedUnreadIndex)
        // }
    
        let groupedReadNotification = []
        let likedReadPostIndex = -1;
        notificationsDb.forEach((notification, index) => {
            if (notification.type == 'liked_post') {
                if (likedReadPostIndex > -1) {
                    groupedReadNotification[likedReadPostIndex].count = groupedReadNotification[likedReadPostIndex].count + 1
                } else {
                    likedReadPostIndex = index
                    notification.count = 1
                    groupedReadNotification.push(notification)
                }
            } else {
                groupedReadNotification.push(notification)
            }
        })


        const notificationsMergedWithCount = [...groupedUnreadNotification, ...groupedReadNotification]
        console.log('notificationsMergedWithCount', notificationsMergedWithCount)
        let notificationsMerged = notificationsMergedWithCount.map(item => item.id)
     
      
        if (req.query.page)
            page = req.query.page
        if (req.query.limit)
            limit = req.query.limit
        const offset = (page - 1) * limit

        let notificationIds = []
        notificationsMerged = _.uniq(notificationsMerged)
        const count = notificationsMerged.length
        console.log(`count is ${count} and offset = ${offset}`)
        if (offset < notificationsMerged.length) {
            notificationIds.push(...notificationsMerged.slice(offset, offset + limit))

            console.log('notificationIds', notificationIds)
            let notifications = await Notification.where('id', 'IN', notificationIds).orderBy('created_at', 'DESC').fetchAll({ require: false });
            notifications = notifications.toJSON()
 
            const postIds = notifications.filter(item => item.entity_type == 'Post').map(item => item.entity_id)
            const followIds = notifications.filter(item => item.entity_type == 'Follow').map(item => item.entity_id)

            console.log('postIds', postIds)
            console.log('followIds', followIds)

            let posts = []
            if (postIds.length > 0) {
                posts = await Post.where('id', 'IN', postIds).fetchAll({ require: false, withRelated: [{ 'user': query => query.column(constants.userFeedFields) }] })//: query => query.select('id', 'user')
                posts = posts.toJSON()
            }

            let follows = []
            if (followIds.length > 0) {
                follows = await Follow.where('id', 'IN', followIds).fetchAll({ require: false, withRelated: [{ 'followee': query => query.column(constants.userFollowFields) }, { 'follower': query => query.column(constants.userFollowFields) }] })//: query => query.select('id', 'follow')
                follows = follows.toJSON()
            }
         //   return res.success({ notifications: notifications })

            notifications.forEach(item => {
                item.count = 1
                if (item.entity_type == 'Post') {
                    item.user = posts.find(post => post.id == item.entity_id).user
                    item.post = posts.find(post => post.id == item.entity_id)
                    if (item.type == 'liked_post') {
                        const findItem = notificationsMergedWithCount.find(notif => item.id == notif.id)
                        if (findItem) {
                            item.count = findItem.count
                        }
                    }
                } else if (item.entity_type == 'Follow') {
                     item.user = follows.find(follow => follow.id == item.entity_id).follower
                     item.follow = follows.find(follow => follow.id == item.entity_id)
                    
                }
            })
            return res.success({ notifications: notifications, count: count })

        } else {
            return res.success({ notifications: notificationIds, count: count })
        }

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}