'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const User = require('../../../models/user');
const constants = require('../../../config/constants');
const Notification = require('../../../models/notification');

module.exports = async (req, res, next) => {
    try {
        const count = await Notification.where({
            user_id: req.user.id,
            read: false,
            active_status: constants.activeStatus.active
        }).count()
        return res.success({ count })
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}