'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const HashTag = require('../../../../models/hashtag');

module.exports = async (req, res, next) => {
    try {

        const body = req.body.hashtag;
        body.created_by = req.user.id;
        body.approved = true;
        const hashDb = await HashTag.where({ name: body.name }).fetch({ require: false });
        if (hashDb)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.HashtagAlreadyExists)));


        const hashtag = await new HashTag().save(body);

        return res.success({ hashtag });


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
