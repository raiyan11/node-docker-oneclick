'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const constants = require('../../../../config/constants');
const HashTag = require('../../../../models/hashtag');

module.exports = async (req, res, next) => {
    try {
        const hashtagId = req.params.id;

        let hashtag = await HashTag.where({ id: hashtagId, active_status: constants.activeStatus.active, approved: false }).fetch({ require: false });

        if (!hashtag)
            return res.serverError(400, ErrorHandler(constants.post.hashtagNotFoundOrApproved));

        hashtag = await hashtag.save({ active_status: constants.activeStatus.deleted });

        return res.success({ hashtag });


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
