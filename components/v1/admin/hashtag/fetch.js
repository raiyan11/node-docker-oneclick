'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const HashTag = require('../../../../models/hashtag');

module.exports = async (req, res, next) => {
    try {
        const query = HashTag.where({ active_status: constants.activeStatus.active })
        const countQuery = HashTag.where({ active_status: constants.activeStatus.active });

        let hashtags;
        if (req.query.approved_status) {
            hashtags = await query.where({ approved: req.query.approved_status })
            countQuery.where({ approved: req.query.approved_status })
        }
        if (req.query.name) {
            hashtags = await query.where('name', 'ILIKE', "%" + req.query.name + "%")
            countQuery.where({ name: req.query.name })
        }

         hashtags = await query.orderBy("updated_at", "DESC").fetchPage({
            require: false,
            offset: (req.query.page - 1) * req.query.limit,
            limit: req.query.limit
        })

        let hashTagsCount = await countQuery.count();

        return res.success({ hashtags, count: hashTagsCount });


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
