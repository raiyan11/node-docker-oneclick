'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const HashTag = require('../../../../models/hashtag');

module.exports = async (req, res, next) => {
    try {

        let hashtagDb = await HashTag.where({ id: req.params.id, active_status: constants.activeStatus.active }).fetch({ require: false });
        if (!hashtagDb)
            return res.serverError(400, ErrorHandler(constants.post.hashtagNotFound));

        const body = req.body.hashtag;

        const hashDb = await HashTag.where({ name: body.name }).fetch({ require: false });
        if (hashDb)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.HashtagAlreadyExists)));

        const hashtag = await HashTag.forge({ id: hashtagDb.id }).save(body);

        return res.success({ hashtag });


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
