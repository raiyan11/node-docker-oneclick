'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const Users = require('../../../../models/user');
const { constant } = require('lodash');

module.exports = async (req, res, next) => {
    try {
        let users;

        /* if (req.query.approved_status) {
             users = await Users.where().where({ active_status: constants.activeStatus.active }).fetchPage({
                 require: false,
                 withRelated: ['following', 'followers', { 'approval': query => query.select('status', 'user_id') }],
                 offset: (req.query.page - 1) * req.query.limit,
                 limit: req.query.limit,
             })
 
 
         } else {*/

        if (req.query.name) {
            users = await Users.where('user_name', 'ILIKE', "%" + req.query.name + "%").orderBy('created_at', 'DESC')
                .fetchPage({
                    require: false,
                    withRelated: ['following', 'followers', { 'approval': query => query.select('status', 'user_id') }],
                    offset: (req.query.page - 1) * req.query.limit,
                    limit: req.query.limit,
                })
        } else
            users = await Users.where('active_status','!=',constants.activeStatus.deleted).orderBy('created_at', 'DESC').fetchPage({
                require: false,
                withRelated: ['following', 'followers', { 'approval': query => query.select('status', 'user_id') }],
                offset: (req.query.page - 1) * req.query.limit,
                limit: req.query.limit,
            })

        //}
        let usersCount = await Users.count();

        return res.success({ users, Count: usersCount });


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
