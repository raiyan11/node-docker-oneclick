'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const User = require('../../../../models/user');

module.exports = async (req, res, next) => {
    try {
        const body =req.body.user;
       
        let user=  await User.where({id:req.params.id,active_status:constants.activeStatus.active}).fetch({require:false});
       
        if(!user)
          return res.serverError(400, ErrorHandler(constants.error.auth.userNotFound));

           user=await User.forge({id:req.params.id}).save({active_status:constants.activeStatus.inactive});
    
            return res.success();
       
 
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
