'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const User = require('../../../../models/user');
const Approval = require("../../../../models/approval");

module.exports = async (req, res, next) => {
    try {
        let userDb = await User.where({ id: req.params.id, active_status: constants.activeStatus.active }).fetch({ require: false, withRelated: ['approval'], columns: ['id'] });
        console.log('userDb',userDb)
        if (!userDb)
            return res.serverError(400, ErrorHandler(constants.error.auth.userNotFound));

        if (!userDb.attributes.approval)
            return res.serverError(400, ErrorHandler(constants.error.auth.approvalNotFound));


        const statusBody = {}
        if (req.body.user.account_approved)
            statusBody.status = constants.approvalStatus.accepted
        else
            statusBody.status = constants.approvalStatus.denied

        await new Approval({ id: userDb.attributes.approval.id }).save(statusBody);

        let user = await User.where({
            id: userDb.id
        }).fetch({ require: false, withRelated: [{ 'approval': query => query.select('status', 'user_id') }] })

        return res.success({ user });


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
