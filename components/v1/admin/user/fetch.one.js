'use strict';
const { ErrorHandler, awsUtil } = require('../../../../lib/utils');
const User = require('../../../../models/user');
const moment = require('moment');
const constants = require('../../../../config/constants');

module.exports = async (req, res, next) => {
  try {
    //Get logged in user
    let user = await User.where({ id: req.params.id, active_status: constants.activeStatus.active }).fetch({ require: false, withRelated: [ 'approval.attachments', {'followers' : query => query.where({ active_status: constants.activeStatus.active})},{'following':query => query.where({ active_status: constants.activeStatus.active})},{'blocked': query => query.where({ active_status: constants.activeStatus.active})}] });
   
    if (!user)
    return res.serverError(400, ErrorHandler(new Error(constants.error.auth.invalidUser)));

    return res.success({ user });
  } catch (error) {
    return res.serverError(500, ErrorHandler(error));
  }
};
