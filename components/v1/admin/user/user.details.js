'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const Users = require('../../../../models/user');

module.exports = async (req, res, next) => {
    try {
        const user = await Users.where({ id: req.params.id }).fetch({ require: false })
        if (!user)
            return res.serverError(400, ErrorHandler(constants.error.auth.userNotFound));

        return res.success({ user: user });

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}