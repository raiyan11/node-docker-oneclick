'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const constants = require('../../../../config/constants');
const Code = require('../../../../models/codes');

module.exports = async (req, res, next) => {
    try {
        const codes = await Code.where({ active_status: constants.activeStatus.active }).fetchAll({ require: false })

        return res.success({ codes: codes });

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}