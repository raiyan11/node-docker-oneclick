'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const constants = require('../../../../config/constants');
const User = require('../../../../models/user');
const Approval = require("../../../../models/approval");
const NotificationService = require('../../../../services/notification.service');

module.exports = async (req, res, next) => {
  try {

    let approval = await Approval.where({
      id: req.params.id,
      active_status: constants.activeStatus.active
    }).fetch({ require: false });

    if (!approval)
      return res.serverError(400, ErrorHandler(constants.error.auth.approvalNotFound));

    let approvalSaveDb = await approval.save({ status: constants.approvalStatus.accepted });

    //send notification
    NotificationService.createNotification(
      approvalSaveDb.id, 'Approval', `Approval request accepted`, `You've been approved. You can post now`, `posting_approved`, approvalSaveDb.attributes.user_id);


    return res.success({ approval: approvalSaveDb });

  } catch (error) {
    return res.serverError(500, ErrorHandler(error));
  }
}
