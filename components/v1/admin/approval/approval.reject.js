'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const Approval = require('../../../../models/approval');
const User = require('../../../../models/user');

module.exports = async (req, res, next) => {
    try {

        let approval = await Approval.query({
            where: { id: req.params.id },
            andWhere: { active_status: constants.activeStatus.active }
        }).fetch({ require: false });

        if (!approval)
            return res.serverError(400, ErrorHandler(constants.error.auth.approvalNotFound));

        let approvalSaveDb = await approval.save({ status: constants.approvalStatus.denied });

        return res.success({ approval: approvalSaveDb });

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
