'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const constants = require('../../../../config/constants');
const User = require('../../../../models/user');
const randomstring = require("randomstring");
const moment = require("moment");
const Code = require('../../../../models/codes');
const sendGrid = require('../../../../lib/utils/sendgrid');

module.exports = async (req, res, next) => {
    try {
        const body = req.body;
        const code = randomstring.generate(6);
        const expiresAt = moment().add(+30, 'days');
        console.log(expiresAt);
        const codeCheck = await Code.where({ email: body.email }).fetch({ require: false })
        if (codeCheck)
            return res.serverError(400, ErrorHandler(constants.error.auth.codeAlreadySent));
        //creating codedb
        await Code.forge().save({ code: code, email: body.email, expires_at: expiresAt })
       
       //ToDo  redirect url
        let shortLink = `${global_Vault_key.url}`;

        // send the code to user email
        await sendGrid.send('Lips - Account Approval Code', 'approval-code', { name: `${body.email}`, code: `${code}` ,shortLink}, [req.body.email]);

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}