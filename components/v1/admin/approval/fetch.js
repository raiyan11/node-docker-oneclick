'use strict';
const { ErrorHandler } = require('../../../../lib/utils');
const { constants } = require('../../../../config');
const Approval = require('../../../../models/approval');

module.exports = async (req, res, next) => {
    try {
        const query = Approval.where({ active_status: constants.activeStatus.active })
        const countQuery = Approval.where({ active_status: constants.activeStatus.active })

        if (req.query.approvedStatus) {
            query.where({ status: req.query.approvedStatus })
            countQuery.where({ status: req.query.approvedStatus })
        }

        const approvals = await query.orderBy("updated_at", "DESC").fetchPage({
            require: false,
            offset: (req.query.page - 1) * req.query.limit,
            limit: req.query.limit,
            withRelated: ['attachments','user']
        })
        // withRelated:[{'approval' : query=> query.select('status','user_id')}]}

        let approvalsCount = await countQuery.count();

        return res.success({ approvals, count: approvalsCount });

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
