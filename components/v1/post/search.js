'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const User = require('../../../models/user');
const Hide = require('../../../models/hide');
const bookshelf = require('../../../config/bookshelf');
const _ = require('lodash');
const moment = require('moment')

module.exports = async (req, res, next) => {
    try {

        const url = fullUrl(req)
        console.log('fullUrl', url)

        const user = req.user;

        const userId = user ? user.id : -1

        const createdAt = req.query.createdAt
        let limit = req.query.limit
        if (!limit) {
            limit = 10
        }

        //This will be universal search for hashtags, title and username
        let search = req.query.search
        if (search)
            search = search.toLowerCase()

        const withRelated = [{ 'user': query => query.column(constants.userFeedFields) },
            , 'attachments', 'hashtagPosts', 'activity',
            { 'is_liked': query => query.where({ user_id: userId})},
            'parent', { 'parent.user': query => query.column(constants.userFeedFields) }, 'parent.attachments', 'parent.hashtagPosts', 'parent.activity', {'reposts': query => query.column('id', 'parent_id', 'user_id').where({user_id: userId}) },    { 'parent.is_liked': query => query.where({ user_id: userId})},{'user.followerSingle' :  query => query.where({follower_id: userId, active_status : constants.activeStatus.active})}]

        const selection = ["posts.id", "users.id as userId"]

        //Posts query variables
        let postIds
        let query
        let countQuery

        //Not logged in user
        if (!user) {
            //Basic post query
            query = bookshelf.knex.select(selection)
                .from("posts")
                .where('posts.active_status', '=', constants.activeStatus.active)
            countQuery = bookshelf.knex.table('posts').where('posts.active_status', '=', constants.activeStatus.active)

            //Adding user to query as for unlogged in we need only public posts
            query.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
                    .andWhere('users.privacy_settings', "=", constants.privacySetting.public)
            })
            countQuery.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
                    .andWhere('users.privacy_settings', "=", constants.privacySetting.public)
            })

            if (search) {
                //search in user_name, hashtags and description at once
                //syntax for search:
                // => unquoted text: text not inside quote marks will be converted to terms separated by & operators.
                // => "quoted text": text inside quote marks will be converted to terms separated by <-> operators.
                // => OR: logical or will be converted to the | operator.
                // => -: the logical not operator, converted to the the ! operator.
                //:* works as ILIKE's %
                // <-> allows us to use this across words
                search = search.replace(/ /g, ":* <-> ");
                search = `${search}:*`;
                query.whereRaw("posts.document @@ to_tsquery(?)", search);
                countQuery.whereRaw("posts.document @@ to_tsquery(?)", search);
            }

            if (createdAt) {
                query.where('posts.created_at', '<', createdAt)
            }

            postIds = await query.orderBy('posts.created_at', 'DESC').limit(req.query.limit)
            console.log('postIds first', postIds)

            if (!postIds || postIds.length == 0)
                return res.success({ posts: [], totalCount: 0 })
            else {
                const posts = await Post.where('id', 'IN', postIds.map(item => item.id)).orderBy('created_at', 'DESC')
                    .fetchAll({
                        require: false,
                        withRelated
                    });
                const latestCreatedAt = posts.toJSON()[posts.toJSON().length - 1].created_at
                const hasNext = await countQuery.where('posts.created_at', '<', latestCreatedAt).count()
                console.log('hasNext', hasNext)
                if (hasNext && hasNext.length > 0 && hasNext[0].count > 0) {
                    let nextPage = `${url}?limit=${limit}&createdAt=${moment(latestCreatedAt).toISOString()}`
                    if (req.query.search) {
                        nextPage = nextPage + `&search=${req.query.search}`
                    }
                    return res.success({ posts: posts, nextPage })
                } else {
                    return res.success({ posts: posts })
                }
            }

        }
        //Logged in user
        else {
            selection.push("users.privacy_settings as userPrivacySettings")


            //Basic post query
            query = bookshelf.knex.select(selection)
                .from("posts")
                .where('posts.active_status', '=', constants.activeStatus.active)
            countQuery = bookshelf.knex.table('posts').where('posts.active_status', '=', constants.activeStatus.active)

            //Adding user to query and checking if user active
            query.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
            })
            countQuery.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
            })

            if (search) {
                //search in user_name, hashtags and description at once
                //syntax for search:
                // => unquoted text: text not inside quote marks will be converted to terms separated by & operators.
                // => "quoted text": text inside quote marks will be converted to terms separated by <-> operators.
                // => OR: logical or will be converted to the | operator.
                // => -: the logical not operator, converted to the the ! operator.
                //:* works as ILIKE's %
                // <-> allows us to use this across words
                search = search.replace(/ /g, ":* <-> ");
                search = `${search}:*`;
                query.whereRaw("posts.document @@ to_tsquery(?)", search);
                countQuery.whereRaw("posts.document @@ to_tsquery(?)", search);
            }

            //Get list of users we do not want to show
            let userData = await User.where({ id: user.id }).fetch({
                columns: ['id'], withRelated: [
                    { hides: query => query.where({ active_status: constants.activeStatus.active }) },
                    { reports: query => query.where({ active_status: constants.activeStatus.active }) },
                    { blocked: query => query.where({ active_status: constants.activeStatus.active }) },
                    { blockedMe: query => query.where({ active_status: constants.activeStatus.active }) },
                    { following: query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }) },
                    { hastagUsers: query => query.where({ active_status: constants.activeStatus.active }) }
                ]
            })
            console.log('userData', userData)


            const avoidUserIds = []
            const avoidPostIds = []
            const followingIds = []

            //Since search can be for both following ids and public
            const publicUserIds = await User.where({ active_status: constants.activeStatus.active, privacy_settings: constants.privacySetting.public }).fetchAll({ require: false, columns: ['id', 'privacy_settings', 'active_status'] })

            avoidPostIds.push(...userData.attributes.hides.map(item => item.post_id))
            avoidPostIds.push(...userData.attributes.reports.map(item => item.post_id))

            avoidUserIds.push(...userData.attributes.blocked.map(item => item.blocked_user_id))
            avoidUserIds.push(...userData.attributes.blockedMe.map(item => item.user_id))

            followingIds.push(...userData.attributes.following.filter(item => item.status == constants.requestStatus.accepted).map(item => item.followee_id))
            followingIds.push(...publicUserIds.toJSON().map(item => item.id))

            console.log('avoidUserIds', avoidUserIds)
            console.log('avoidPostIds', avoidPostIds)
            console.log('followingIds', followingIds)

            if (userData.attributes.hastagUsers) {
                const userHiddenHashtags = userData.attributes.hastagUsers.filter(item => !item.show).map(item => item.hashtag_name)
                console.log('userHiddenHashtags', userHiddenHashtags)
                userHiddenHashtags.forEach(item => {
                    query.where('posts.hashtags', 'NOT LIKE', `%${item}%`)
                    countQuery.where('posts.hashtags', 'NOT LIKE', `%${item}%`)
                })
            }

            query.whereNotIn('posts.id', avoidPostIds).whereIn('users.id', followingIds).whereNotIn('users.id', avoidUserIds)

            if (createdAt) {
                query.where('posts.created_at', '<', createdAt)
            }

            //Fetch posts
            postIds = await query.orderBy('posts.created_at', 'DESC')
                .limit(req.query.limit)
            countQuery.whereNotIn('posts.id', avoidPostIds).whereIn('users.id', followingIds).whereNotIn('users.id', avoidUserIds)
            console.log('postIds first', postIds)


            if (!postIds || postIds.length == 0)
                return res.success({ posts: [], totalCount: 0 })
            else {
                const posts = await Post.where('id', 'IN', postIds.map(item => item.id)).orderBy('created_at', 'DESC')
                    .fetchAll({
                        require: false,
                        withRelated
                    });
                const latestCreatedAt = posts.toJSON()[posts.toJSON().length - 1].created_at
                const hasNext = await countQuery.where('posts.created_at', '<', latestCreatedAt).count()
                console.log('hasNext', hasNext)
                if (hasNext && hasNext.length > 0 && hasNext[0].count > 0) {
                    let nextPage = `${url}?limit=${limit}&createdAt=${moment(latestCreatedAt).toISOString()}`
                    if (req.query.search) {
                        nextPage = nextPage + `&search=${req.query.search}`
                    }
                    return res.success({ posts: posts, nextPage })
                } else {
                    return res.success({ posts: posts })
                }
            }

        }


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}


