'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const User = require('../../../models/user');
const bookshelf = require('../../../config/bookshelf');
const moment = require('moment')


module.exports = async (req, res, next) => {
    try {

        const url = fullUrl(req)
        console.log('fullUrl', url)

        let limit = req.query.limit
        const createdAt = req.query.createdAt
        if (!limit) {
            limit = 10
        }
        let userId = -1

        const userAuth = req.user;
        if (!userAuth) {

            let otherUser = await User.where({ id: req.params.id, active_status: constants.activeStatus.active }).fetch({ require: false, columns: ['id', 'privacy_settings'] });
            console.log("otherUser", otherUser.toJSON())
            if (!otherUser)
                return res.serverError(400, ErrorHandler(new Error(constants.error.auth.invalidUser)));
            else if (otherUser.toJSON().privacy_settings != constants.privacySetting.public)
                return res.serverError(400, ErrorHandler(new Error(constants.error.auth.userIsNotPublic)));
        } else {
            userId = userAuth.id
        }

        const query = bookshelf.knex.select("posts.id")
            .from("posts")
            .where('posts.user_id', '=', req.params.id).andWhere('posts.active_status', '=', constants.activeStatus.active)


        const countQuery = bookshelf.knex.table('posts').where('posts.user_id', '=', req.params.id).where(function () {
            this.where('posts.active_status', '=', constants.activeStatus.active).orWhere('posts.active_status', '=', constants.activeStatus.hidden)
        })


        if (createdAt) {
            query.where('posts.created_at', '<', createdAt)
        }

        const postIds = await query.orderBy('posts.created_at', 'DESC').limit(limit)

        const posts = await Post.where('id', 'IN', postIds.map(item => item.id)).orderBy('created_at', 'DESC')
            .fetchAll({
                require: false,
                withRelated: [{ 'user': query => query.column(constants.userFeedFields) }, 'attachments', 'hashtagPosts', 'activity', { 'is_liked': query => query.where({ user_id: userId }) },
                    'parent', { 'parent.user': query => query.column(constants.userFeedFields) }, 'parent.attachments', 'parent.hashtagPosts', 'parent.activity', { 'parent.is_liked': query => query.where({ user_id: userId }) }, { 'reposts': query => query.column('id', 'parent_id', 'user_id').where({ user_id: userId }) }, { 'user.followerSingle': query => query.where({ follower_id: userId, active_status: constants.activeStatus.active }) }]
            });

     if(posts.length!=0){
        const latestCreatedAt = posts.toJSON()[posts.toJSON().length - 1].created_at
        const hasNext = await countQuery.where('posts.created_at', '<', latestCreatedAt).count()
        console.log('hasNext', hasNext)
        if (hasNext && hasNext.length > 0 && hasNext[0].count > 0) {
            const nextPage = `${url}?limit=${limit}&createdAt=${moment(latestCreatedAt).toISOString()}`
            return res.success({ posts: posts, nextPage })
        } else {
            return res.success({ posts: posts })
        }}
    else
    return res.success({ posts: posts })
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}