'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const Like = require('../../../models/like');

module.exports = async (req, res, next) => {
    try {

        const postId = req.params.id;

        const postChk = await Post.where({ id: postId }).count();
        if (postChk == 0)
            return res.serverError(400, ErrorHandler(constants.post.notFound));

        // check if liked this post or not
        const likeChk = await Like.where({ post_id: postId, active_status: constants.activeStatus.active }).fetchAll({
            require: false, offset: (req.query.page - 1) * req.query.limit,
            limit: req.query.limit, withRelated: [{ 'user': query => query.column(constants.userFeedFields) }]
        });

        const count = await Like.where({ post_id: postId, active_status: constants.activeStatus.active }).count()
        console.log(likeChk.length)

        let users = likeChk.toJSON().map(userpost => userpost.user);
        return res.success({ users, totalCount:count });
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
