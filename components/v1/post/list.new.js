'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const User = require('../../../models/user');
const Hide = require('../../../models/hide');
const bookshelf = require('../../../config/bookshelf');
const _ = require('lodash')
const moment = require('moment')

module.exports = async (req, res, next) => {
    try {
        const user = req.user;
        const selection = ["posts.id", "users.id as usersId"]

        //Posts query variables
        let countQuery

        const firstItemCreatedAt = req.query.firstItemCreatedAt

        //Not logged in user
        if (!user) {

            //Basic post query
            countQuery = bookshelf.knex.table('posts').where('posts.active_status', '=', constants.activeStatus.active)

            //Adding user to query as for unlogged in we need only public posts
            countQuery.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
                    .andWhere('users.privacy_settings', "=", constants.privacySetting.public)
            })

            const count = await countQuery.where('posts.created_at', '>', firstItemCreatedAt).count()

            return res.success({ hasNew: count && count.length > 0 && count[0].count > 0 })

        }
        //Logged in user
        else {

            selection.push("posts.hashtags as postHashtags")
            //Basic post query
            countQuery = bookshelf.knex.table('posts').where('posts.active_status', '=', constants.activeStatus.active)

            //Adding user to query and checking if user active
            countQuery.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
            })

            let userData = await User.where({ id: user.id }).fetch({
                columns: ['id'], withRelated: [
                    { hides: query => query.where({ active_status: constants.activeStatus.active }) },
                    { reports: query => query.where({ active_status: constants.activeStatus.active }) },
                    { blocked: query => query.where({ active_status: constants.activeStatus.active }) },
                    { blockedMe: query => query.where({ active_status: constants.activeStatus.active }) },
                    { following: query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }) },
                    { hastagUsers: query => query.where({ active_status: constants.activeStatus.active }) }
                ]
            })

            const avoidUserIds = []
            const avoidPostIds = []
            const followingIds = []

            avoidPostIds.push(...userData.attributes.hides.map(item => item.post_id))
            avoidPostIds.push(...userData.attributes.reports.map(item => item.post_id))


            if (userData.attributes.hastagUsers) {
                const userHiddenHashtags = userData.attributes.hastagUsers.filter(item => !item.show).map(item => item.hashtag_name)
                userHiddenHashtags.forEach(item => {
                    countQuery.where('posts.hashtags', 'NOT LIKE', `%${item}%`)
                })
            }

            avoidUserIds.push(...userData.attributes.blocked.map(item => item.blocked_user_id))
            avoidUserIds.push(...userData.attributes.blockedMe.map(item => item.user_id))

            followingIds.push(...userData.attributes.following.filter(item => item.status == constants.requestStatus.accepted).map(item => item.followee_id))

            //Fetch posts
            countQuery.whereNotIn('posts.id', avoidPostIds).whereIn('users.id', followingIds).whereNotIn('users.id', avoidUserIds)


            const count = await countQuery.where('posts.created_at', '>', firstItemCreatedAt).count()

            return res.success({ hasNew: count && count.length > 0 && count[0].count > 0 })

        }

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}


