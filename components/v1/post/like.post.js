'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const Like = require('../../../models/like');
const Activity = require('../../../models/activity');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');
const NotificationService = require('../../../services/notification.service');

module.exports = async (req, res, next) => {
    try {

        var postId = req.params.id;

        let postChk = await Post.where({ id: postId }).fetch({ require: false });
        if (!postChk)
            return res.serverError(400, ErrorHandler(constants.post.notFound));

        var likeObj = {
            user_id: req.user.id,
            post_id: postId
        }

        // prevent user from liking same post more than once
        let likeChk = await Like.where({ post_id: postId, user_id: req.user.id }).fetch({ require: false });
        console.log('likeChk', likeChk)
        if (likeChk)
            return res.serverError(400, ErrorHandler(constants.post.alreadyLikedByUser));

        await new Like().save(likeObj);

        // increment count in Activity tbl
        await Activity.query().where('post_id', postId).increment('likes_count', 1);

        //send notification
        if (postChk.toJSON().user_id != req.user.id)
            NotificationService.createNotification(
                postId, 'Post', `Your post is liked`, `${req.user.user_name} liked your post`, `liked_post`, postChk.attributes.user_id);

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
