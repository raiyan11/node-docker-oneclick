
'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const User = require('../../../models/user');
const Hide = require('../../../models/hide');
const bookshelf = require('../../../config/bookshelf');
const _ = require('lodash')
const moment = require('moment')

module.exports = async (req, res, next) => {
    try {

        const url = fullUrl(req)
        console.log('fullUrl', url)

        let userId = -1
        if (req.user)
            userId = req.user.id
        const withRelated = [{ 'user': query => query.column(constants.userFeedFields) },
            , 'attachments', 'hashtagPosts', 'activity',
            'parent', { 'parent.user': query => query.column(constants.userFeedFields) }, 'parent.attachments', 'parent.hashtagPosts', 'parent.activity', { 'reposts': query => query.column('id', 'parent_id', 'user_id').where({user_id: userId}) }, { 'user.followerSingle': query => query.where({ follower_id: userId, active_status: constants.activeStatus.active }) },
        ]

        const selection = ["posts.id", "users.id as usersId"]

        //Posts query variables
        let postIds
        let query
        let countQuery

        const createdAt = req.query.createdAt
        let limit = req.query.limit
        if (!limit) {
            limit = 10
        }

        console.log('limit', limit)

        let search = req.query.search
        if (search)
            search = search.toLowerCase()

        const hideHashtags = req.body.hashtags.hide
        const showHashtags = req.body.hashtags.show

        //Basic post query
        query = bookshelf.knex.select(selection)
            .from("posts")
            .where('posts.active_status', '=', constants.activeStatus.active)
        countQuery = bookshelf.knex.table('posts').where('posts.active_status', '=', constants.activeStatus.active)

        //Adding user to query as for unlogged in we need only public posts
        query.leftOuterJoin("users", function () {
            this.on("users.id", "=", "posts.user_id");
        }).where(function () {
            this.where('users.active_status', '=', constants.activeStatus.active)
                .andWhere('users.privacy_settings', "=", constants.privacySetting.public)
        })
        countQuery.leftOuterJoin("users", function () {
            this.on("users.id", "=", "posts.user_id");
        }).where(function () {
            this.where('users.active_status', '=', constants.activeStatus.active)
                .andWhere('users.privacy_settings', "=", constants.privacySetting.public)
        })

        if (search) {
            //search in user_name, hashtags and description at once
            //syntax for search:
            // => unquoted text: text not inside quote marks will be converted to terms separated by & operators.
            // => "quoted text": text inside quote marks will be converted to terms separated by <-> operators.
            // => OR: logical or will be converted to the | operator.
            // => -: the logical not operator, converted to the the ! operator.
            //:* works as ILIKE's %
            // <-> allows us to use this across words
            search = search.replace(/ /g, ":* <-> ");
            search = `${search}:*`;
            query.whereRaw("posts.document @@ to_tsquery(?)", search);
            countQuery.whereRaw("posts.document @@ to_tsquery(?)", search);
        }

        if (showHashtags && showHashtags.length > 0) {
            query.where(function () {
                this.where('posts.hashtags', 'LIKE', `%${showHashtags[0]}%`)
                for (var i = 1; i < showHashtags.length; i++) {
                    this.orWhere('posts.hashtags', 'LIKE', `%${showHashtags[i]}%`)
                }
            })
        }

        if (hideHashtags && hideHashtags.length > 0) {
            query.where(function () {
                this.where('posts.hashtags', 'NOT LIKE', `%${hideHashtags[0]}%`)
                for (var i = 1; i < hideHashtags.length; i++) {
                    this.orWhere('posts.hashtags', 'NOT LIKE', `%${hideHashtags[i]}%`)
                }
            })
        }

        if (createdAt) {
            query.where('posts.created_at', '<', createdAt)
        }


        postIds = await query.orderBy('posts.created_at', 'DESC').limit(req.query.limit)
        console.log('postIds first', postIds)

        if (!postIds || postIds.length == 0)
            return res.success({ posts: [], totalCount: 0 })
        else {
            const posts = await Post.where('id', 'IN', postIds.map(item => item.id)).orderBy('created_at', 'DESC')
                .fetchAll({
                    require: false,
                    withRelated
                });
            const latestCreatedAt = posts.toJSON()[posts.toJSON().length - 1].created_at
            const hasNext = await countQuery.where('posts.created_at', '<', latestCreatedAt).count()
            console.log('hasNext', hasNext)
            if (hasNext && hasNext.length > 0 && hasNext[0].count > 0) {
                let nextPage = `${url}?limit=${limit}&createdAt=${moment(latestCreatedAt).toISOString()}`
                if (req.query.search) {
                    nextPage = nextPage + `&search=${req.query.search}`
                }
                return res.success({ posts: posts, nextPage })
            } else {
                return res.success({ posts: posts })
            }
        }

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}