'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const HashtagPost = require('../../../models/hashtag.post');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');
const Attachment = require('../../../models/attachment');

module.exports = async (req, res, next) => {
    try {
        let post = await Post.where({ id: req.params.id }).fetch({ require: false, withRelated: ['attachments', 'hashtagPosts', { 'parent': query => query.select('id','user_id') }] });
        if (!post)
            return res.serverError(400, ErrorHandler(constants.post.notFound));
        //Check if post is a repost
        if (post.toJSON().type == constants.postType.repost && post.toJSON().parent.user_id) {
            console.log('type is repost')
            console.log(`parent user id is ${post.toJSON().parent.user_id} and post user_id is ${post.toJSON().user_id}`)
            if (post.toJSON().parent.user_id != req.user.id && post.toJSON().user_id != req.user.id)
                return res.serverError(400, ErrorHandler(constants.post.postNotUsers));
        } else if (post.toJSON().user_id != req.user.id) {
            console.log('type is not repost')
            return res.serverError(400, ErrorHandler(constants.post.postNotUsers));
        }


        //Soft delete post
        await Post.forge({ id: req.params.id }).save({ active_status: constants.activeStatus.deleted });

        //TODO - ask if we need to delete the repost also

        //delete the repost
        /*let repostData = await Post.query({
            where: { parent_id: req.params.id },
        }).fetchAll({ require: false, columns: ['id'] })


        repostData = repostData.toJSON().map(repost => {
            const obj = { id: repost.id, active_status: constants.activeStatus.deleted }
            repost.id
            return obj
        })

        await Post.collection(repostData).invokeThen('save');*/

        //Soft deleting hashtags
        const hashTagPosts = post.toJSON().hashtagPosts
        if (hashTagPosts.length > 0) {
            const postHashtagList = []
            hashTagPosts.forEach(item => {
                const hashTagObj = {
                    id: item.id,
                    active_status: constants.activeStatus.deleted
                }
                postHashtagList.push(hashTagObj)
            })
            await HashtagPost.collection(postHashtagList).invokeThen('save');
        }

        //Soft deleting attachments
        const attachments = post.toJSON().attachments
        if (attachments.length > 0) {
            const attachmentsList = []
            attachments.forEach(item => {
                const attachmentObject = {
                    id: item.id,
                    active_status: constants.activeStatus.deleted
                }
                attachmentsList.push(attachmentObject)
            })
            await Attachment.collection(attachmentsList).invokeThen('save');
        }

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
