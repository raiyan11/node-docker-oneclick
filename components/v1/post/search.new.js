'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const User = require('../../../models/user');
const Hide = require('../../../models/hide');
const bookshelf = require('../../../config/bookshelf');
const _ = require('lodash');
const moment = require('moment')

module.exports = async (req, res, next) => {
    try {
        const user = req.user;

        const firstItemCreatedAt = req.query.firstItemCreatedAt
        //This will be universal search for hashtags, title and username
        let search = req.query.search
        if (search)
            search = search.toLowerCase()


        const selection = ["posts.id", "users.id as userId"]

        //Posts query variables
        let countQuery

        //Not logged in user
        if (!user) {
            countQuery = bookshelf.knex.table('posts').where('posts.active_status', '=', constants.activeStatus.active)

            //Basic post query
            countQuery.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
                    .andWhere('users.privacy_settings', "=", constants.privacySetting.public)
            })

            if (search) {
                //search in user_name, hashtags and description at once
                //syntax for search:
                // => unquoted text: text not inside quote marks will be converted to terms separated by & operators.
                // => "quoted text": text inside quote marks will be converted to terms separated by <-> operators.
                // => OR: logical or will be converted to the | operator.
                // => -: the logical not operator, converted to the the ! operator.
                //:* works as ILIKE's %
                // <-> allows us to use this across words
                search = search.replace(/ /g, ":* <-> ");
                search = `${search}:*`;
                countQuery.whereRaw("posts.document @@ to_tsquery(?)", search);
            }
            const count = await countQuery.where('posts.created_at', '>', firstItemCreatedAt).count()

            return res.success({ hasNew: count && count.length > 0 && count[0].count > 0 })
        }
        //Logged in user
        else {
            selection.push("users.privacy_settings as userPrivacySettings")

            countQuery = bookshelf.knex.table('posts').where('posts.active_status', '=', constants.activeStatus.active)

            countQuery.leftOuterJoin("users", function () {
                this.on("users.id", "=", "posts.user_id");
            }).where(function () {
                this.where('users.active_status', '=', constants.activeStatus.active)
            })

            if (search) {
                //search in user_name, hashtags and description at once
                //syntax for search:
                // => unquoted text: text not inside quote marks will be converted to terms separated by & operators.
                // => "quoted text": text inside quote marks will be converted to terms separated by <-> operators.
                // => OR: logical or will be converted to the | operator.
                // => -: the logical not operator, converted to the the ! operator.
                //:* works as ILIKE's %
                // <-> allows us to use this across words
                search = search.replace(/ /g, ":* <-> ");
                search = `${search}:*`;
                countQuery.whereRaw("posts.document @@ to_tsquery(?)", search);
            }

            //Get list of users we do not want to show
            let userData = await User.where({ id: user.id }).fetch({
                columns: ['id'], withRelated: [
                    { hides: query => query.where({ active_status: constants.activeStatus.active }) },
                    { reports: query => query.where({ active_status: constants.activeStatus.active }) },
                    { blocked: query => query.where({ active_status: constants.activeStatus.active }) },
                    { blockedMe: query => query.where({ active_status: constants.activeStatus.active }) },
                    { following: query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }) },
                    { hastagUsers: query => query.where({ active_status: constants.activeStatus.active }) }
                ]
            })
            console.log('userData', userData)


            const avoidUserIds = []
            const avoidPostIds = []
            const followingIds = []

            //Since search can be for both following ids and public
            const publicUserIds = await User.where({ active_status: constants.activeStatus.active, privacy_settings: constants.privacySetting.public }).fetchAll({ require: false, columns: ['id', 'privacy_settings', 'active_status'] })

            avoidPostIds.push(...userData.attributes.hides.map(item => item.post_id))
            avoidPostIds.push(...userData.attributes.reports.map(item => item.post_id))

            avoidUserIds.push(...userData.attributes.blocked.map(item => item.blocked_user_id))
            avoidUserIds.push(...userData.attributes.blockedMe.map(item => item.user_id))

            followingIds.push(...userData.attributes.following.filter(item => item.status == constants.requestStatus.accepted).map(item => item.followee_id))
            followingIds.push(...publicUserIds.toJSON().map(item => item.id))

            console.log('avoidUserIds', avoidUserIds)
            console.log('avoidPostIds', avoidPostIds)
            console.log('followingIds', followingIds)

            if (userData.attributes.hastagUsers) {
                const userHiddenHashtags = userData.attributes.hastagUsers.filter(item => !item.show).map(item => item.hashtag_name)
                console.log('userHiddenHashtags', userHiddenHashtags)
                userHiddenHashtags.forEach(item => {
                    countQuery.where('posts.hashtags', 'NOT LIKE', `%${item}%`)
                })
            }

            countQuery.whereNotIn('posts.id', avoidPostIds).whereIn('users.id', followingIds).whereNotIn('users.id', avoidUserIds)

            const count = await countQuery.where('posts.created_at', '>', firstItemCreatedAt).count()

            return res.success({ hasNew: count && count.length > 0 && count[0].count > 0 })

        }


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}


