'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Block = require('../../../models/block');
const Like = require('../../../models/like');
const bookshelf = require('../../../config/bookshelf');
const moment = require('moment')

module.exports = async (req, res, next) => {
    try {

        const url = fullUrl(req)
        console.log('fullUrl', url)

        const createdAt = req.query.createdAt

        const query = bookshelf.knex.select("likes.id")
            .from("likes")
            .where('likes.active_status', '=', constants.activeStatus.active).andWhere('likes.user_id', '=', req.user.id)

        const countQuery = bookshelf.knex.table('likes')
            .where('likes.active_status', '=', constants.activeStatus.active).andWhere('likes.user_id', '=', req.user.id)

        let limit = req.query.limit
        if (!limit) {
            limit = 10
        }

        let blockedUsers = await Block.where({ user_id: req.user.id, active_status: constants.activeStatus.active  }).fetchAll({ require: false });
        blockedUsers = blockedUsers.toJSON()
        blockedUsers = blockedUsers.map(item => item.blocked_user_id)
        console.log('blockedUsers', blockedUsers)

        query.leftOuterJoin("posts", function () {
            this.on("posts.id", "=", "likes.post_id");
        }).where(function () {
            this.where('posts.active_status', '=', constants.activeStatus.active)
                .andWhere('posts.user_id', 'NOT IN', blockedUsers)
        })

        countQuery.leftOuterJoin("posts", function () {
            this.on("posts.id", "=", "likes.post_id");
        }).where(function () {
            this.where('posts.active_status', '=', constants.activeStatus.active)
                .andWhere('posts.user_id', 'NOT IN', blockedUsers)
        })

        if (createdAt) {
            query.where('likes.created_at', '<', createdAt)
        }

        const likeIds = await query.orderBy('likes.created_at', 'desc').limit(limit);

        console.log('likeIds', likeIds)

        let likes = await Like.where('id', 'IN', likeIds.map(item => item.id))
            .orderBy('created_at', 'DESC')
            .fetchAll({
                require: false,
                withRelated: ['post', 'post.attachments', 'post.hashtagPosts', 'post.activity', { 'post.user': query => query.column(constants.userFeedFields) },
                    , 'post.parent', { 'post.parent.user': query => query.column(constants.userFeedFields) }, 'post.parent.attachments', 'post.parent.hashtagPosts', 'post.parent.activity', { 'post.parent.is_liked': query => query.where({ user_id: req.user.id }) },
                    { 'post.reposts': query => query.column('id', 'parent_id', 'user_id').where({ user_id: req.user.id }) }, { 'post.user.followerSingle': query => query.where({ follower_id: req.user.id, active_status: constants.activeStatus.active }) }]
            });


        likes = likes.toJSON();
        let latestCreatedAt;
        if(likes.length!=0){
          latestCreatedAt = likes[likes.length - 1].created_at

        likes = likes.map(item => {
            const obj = { ...item.post, liked: true }
            return obj
        })
        const hasNext = await countQuery.where('likes.created_at', '<', latestCreatedAt).count()
        console.log('hasNext', hasNext)
        if (hasNext && hasNext.length > 0 && hasNext[0].count > 0) {
            const nextPage = `${url}?limit=${limit}&createdAt=${moment(latestCreatedAt).toISOString()}`
            return res.success({ posts: likes, nextPage })
        }
        return res.success({ posts: likes })
      
        } else {
            return res.success({ posts: likes })
        }


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}