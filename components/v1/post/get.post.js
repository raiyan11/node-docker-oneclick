'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const HashTagPost = require('../../../models/hashtag.post');
const HashTags = require('../../../models/hashtag');
const Activity = require('../../../models/activity');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');

module.exports = async (req, res, next) => {
    try {

        const userAuth = req.user;
        let post ;
        if (!userAuth) {
             post = await Post.where({ id: req.params.id }).fetch({
                require: false,
                withRelated: [{ 'user': query => query.column(constants.userFeedFields) }
                ,'attachments', 'hashtagPosts', 'activity',
                'is_liked',
                'parent',
                {'parent.user' : query => query.column(constants.userFeedFields)}, 
                'parent.attachments', 
                    'parent.hashtagPosts', 
                'parent.activity', 
                'parent.is_liked',
                {'reposts': query => query.column('id', 'parent_id', 'user_id').where({user_id: -1}) }]
            });
            if (!post)
             return res.serverError(400, ErrorHandler(constants.post.notFound));
            if(post.toJSON().user.privacy_settings == constants.privacySetting.private)
              return res.serverError(400, ErrorHandler(constants.post.privatePost));
           
        }else{
            //TODO -check if user is blocked
             post = await Post.where({ id: req.params.id }).fetch({
                require: false,
                withRelated: [{ 'user': query => query.column(constants.userFeedFields) }
                    , 'attachments', 'hashtagPosts', 'activity',
                { 'is_liked': query => query.where({ user_id: req.user.id }) },
                'parent',{'parent.user' : query => query.column(constants.userFeedFields)}, 'parent.attachments', 'parent.hashtagPosts', 'parent.activity', { 'parent.is_liked': query => query.where({ user_id: req.user.id }) } ,
                {'reposts': query => query.column('id', 'parent_id', 'user_id').where({user_id: req.user.id}) },
                {'user.followerSingle' :  query => query.where({follower_id: req.user.id, active_status : constants.activeStatus.active})}]
            });
         
        }
        if (!post)
            return res.serverError(400, ErrorHandler(constants.post.notFound));

        return res.success({ post });
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
