'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const HashtagPost = require('../../../models/hashtag.post');
const Activity = require('../../../models/activity');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');
const NotificationService = require('../../../services/notification.service');
const Attachment = require('../../../models/attachment');
const Approval = require('../../../models/approval');

module.exports = async (req, res, next) => {
    try {

        let postId = req.params.id;

        const post = await Post.where({ id: postId }).fetch({ require: false, withRelated: ['attachments', 'hashtagPosts'] });
        if (!post)
            return res.serverError(400, ErrorHandler(constants.post.notFound));

        // chk if same post is reposted already by this user
        /*let repost = await Post.where({parent_id:post_Id,user_id:req.user.id}).fetch({ require: false });
        if(repost)
         return res.serverError(400, ErrorHandler(constants.error.post.duplicateRepost));*/


        // chk if account approved throw err
        //Get approval
        /*let approval = await Approval.where({
            user_id: req.user.id,
            active_status: constants.activeStatus.active
        }).fetch({ require: false });

        // check if account approved
        if (!approval || approval.toJSON().status != constants.approvalStatus.accepted)
            return res.serverError(400, ErrorHandler(constants.post.accountNotApproved));*/

        const hashTagPosts = post.toJSON().hashtagPosts;
        const attachments = post.toJSON().attachments

        let postJson = post.toJSON();
        delete postJson.id;
        delete postJson.attachments
        delete postJson.hashtagPosts


        postJson.user_id = req.user.id;

        if(postJson.parent_id){
            postJson.parent_id = postJson.parent_id;
        }else{
            postJson.parent_id = postId;
        }

        postJson.type = 'repost';
        postJson.user_name = req.user.user_name

        delete postJson.created_at
        delete postJson.updated_at

        //inserting post to post table
        const repost = await Post.forge().save(postJson);
        const repostId = repost.toJSON().id; //post_id from inserted post

        // Add a record for this post in activity table
        var activityObj = {
            likes_count: 0,
            post_id: repostId
        }
        await Activity.forge(activityObj).save();


        // Attaching hashtags to reposted
        if (hashTagPosts.length > 0) {
            const postHashtagList = []
            hashTagPosts.forEach(item => {
                const hashTagObj = {
                    hashtag_name: item.id, //TODO:what is logic behind this ?
                    post_id: repostId
                }
                postHashtagList.push(hashTagObj)
            })
            await HashtagPost.collection(postHashtagList).invokeThen('save');
        }

        // Attaching attachments to reposted
        if (attachments.length > 0) {
            const attachmentsList = []
            attachments.forEach(item => {
                const attachmentObject = {
                    entity_type: 'Post',
                     width: item.width,
                     height: item.height,
                     entity_id: repost.id,
                    url: item.url
                }
                attachmentsList.push(attachmentObject)
            })
            await Attachment.collection(attachmentsList).invokeThen('save');
        }

        //Response
        const postRes = await Post.where({ id: repostId }).fetch({
            require: false,
            withRelated: ['attachments', 'hashtagPosts', 'activity',{'user': query => query.column(constants.userFeedFields)},
            'parent',{'parent.user' : query => query.column(constants.userFeedFields)}, 'parent.attachments', 'parent.hashtagPosts', 'parent.activity', { 'parent.is_liked': query => query.where({ user_id: req.user.id }) } , {'reposts': query => query.column('id', 'parent_id', 'user_id').where({user_id: req.user.id})}]
        });

         //send notification
         NotificationService.createNotification(
            repostId, 'Post', `Your post is shared`, `${req.user.user_name} shared your post`, `shared_your_post`, post.attributes.user_id);

        return res.success({ post: postRes });
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
