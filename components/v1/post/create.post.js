'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const HashtagPost = require('../../../models/hashtag.post');
const HashTag = require('../../../models/hashtag');
const Activity = require('../../../models/activity');
const Attachment = require('../../../models/attachment');
const Approval = require('../../../models/approval')
const _ = require('lodash')

module.exports = async (req, res, next) => {
    try {

        console.log('Creating post')

        const user = req.user

        console.log('checking approval')


        //Get approval
        let approval = await Approval.where({
            user_id: user.id,
            active_status: constants.activeStatus.active
        }).fetch({ require: false });

        // check if account approved
        if (!approval || approval.toJSON().status != constants.approvalStatus.accepted) {
            return res.serverError(400, ErrorHandler(constants.post.accountNotApproved));
        }

        console.log('checking hashtags')

        //Check all hashtags
        let hashTags = req.body.hashTags;
        if (hashTags) {
            hashTags = _.uniq(hashTags)
            const hashTagsCount = await HashTag.where('name', 'IN', hashTags).where({ active_status: constants.activeStatus.active }).count()
            if (hashTagsCount != hashTags.length) {
                return res.serverError(400, ErrorHandler(constants.post.hashtagsInvalid));
            }
        }



        //Extracting photo paths and saving post
        const postBody = req.body.post;
        const photoPaths = postBody.photo_paths;
        delete postBody.photo_paths;
        postBody.user_id = req.user.id;
        if (hashTags)
            postBody.hashtags = hashTags.join(',')
        postBody.user_name = user.user_name
        const post = await Post.forge(postBody).save();

        // Creating record in activity table
        var activityObj = {
            likes_count: 0,
            post_id: post.id
        }
        await Activity.forge(activityObj).save();

        console.log('Saving hashtags')

        //Saving hashtags
        if (hashTags && hashTags.length > 0) {
            const postHashtagList = []
            hashTags.forEach(item => {
                const hashTagObj = {
                    hashtag_name: item,
                    post_id: post.id
                }
                postHashtagList.push(hashTagObj)
            })
            //increment like count
            hashTags.forEach(async item => {
                const increment = await HashTag.query().where('name', '=', item).increment('count', 1);
                console.log(`increment for ${item} is ${increment}`)
            })
            await HashtagPost.collection(postHashtagList).invokeThen('save');
        }

        console.log('Saving photo paths')
        //Saving photopaths
        if (photoPaths && photoPaths.length > 0) {
            let attachments = [];
            photoPaths.forEach((photoPath) => {
                attachments.push({
                    entity_type: 'Post',
                    entity_id: post.id,
                    ...photoPath
                });
            });
            
            console.log('attachments', attachments)
            // save photo paths
           await Attachment.collection(attachments).invokeThen('save');
        }

        console.log('fetching saved post')

        //Response
        const postRes = await Post.where({ id: post.id }).fetch({
            require: false,
            withRelated: [{ 'user': query => query.column(constants.userFeedFields) }, 'attachments', 'hashtagPosts', 'activity']
        });

        console.log('returning post')

        return res.success({ post: postRes });
    } catch (error) {
        console.log('error', error)
        return res.serverError(500, ErrorHandler(error));
    }
};
