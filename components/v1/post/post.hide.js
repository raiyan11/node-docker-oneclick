'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const Hide = require('../../../models/hide');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');

module.exports = async (req, res, next) => {
    try {

        var postId = req.params.id;

        let postChk = await Post.where({ id: postId }).fetch({ require: false });
        if (!postChk)
            return res.serverError(400, ErrorHandler(constants.post.notFound));
        let hide = await Hide.where({ user_id: req.user.id, post_id: postId }).fetch({ require: false });
        if (!hide)
            await new Hide().save({ user_id: req.user.id, post_id: postId });

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
