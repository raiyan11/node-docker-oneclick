'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');

module.exports = async (req, res, next) => {
    try {

        let postCheck = await Post.where({ id: req.params.id }).fetch({ require: false });
        if (!postCheck)
            return res.serverError(400, ErrorHandler(constants.post.notFound));
        if (postCheck.toJSON().user_id != req.user.id)
            return res.serverError(400, ErrorHandler(constants.post.notBelongToUser));

        await Post.forge().where({ id: req.params.id, user_id: req.user.id }).save({ active_status: constants.activeStatus.active }, { method: 'update' });

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
