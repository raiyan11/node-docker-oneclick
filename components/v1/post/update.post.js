'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const HashtagPost = require('../../../models/hashtag.post');
const Attachment = require('../../../models/attachment');
const HashTag = require('../../../models/hashtag');
const _ = require('lodash')


module.exports = async (req, res, next) => {
    try {

        //Check post
        let postCheck = await Post.where({ id: req.params.id }).fetch({ require: false, withRelated: ['hashtagPosts'] });
        if (!postCheck)
            return res.serverError(400, ErrorHandler(constants.post.notFound));
        if (postCheck.toJSON().user_id != req.user.id) {
            return res.serverError(400, ErrorHandler(constants.post.postNotUsers));
        }
        if (postCheck.toJSON().type == constants.postType.repost) {
            return res.serverError(400, ErrorHandler(constants.post.cannotUpdateRepost));
        }

        //Checking hashtags
        let hashTags = req.body.hashTags;
        if (hashTags) {
            hashTags = _.uniq(hashTags)
            const hashTagsCount = await HashTag.where('name', 'IN', hashTags).where({ active_status: constants.activeStatus.active }).count()
            if (hashTagsCount != hashTags.length) {
                return res.serverError(400, ErrorHandler(constants.post.hashtagsInvalid));
            }
        }
        let removeHashTags = req.body.removeHashTags;
        console.log('removeHashTags', removeHashTags)

        //Updating post
        const postBody = req.body.post;
        const photoPaths = postBody.photo_paths;
        const deleteAttachments = postBody.remove_attachments
        delete postBody.photo_paths;
        delete postBody.remove_attachments;

        //Get final list of hashtags 
        let finalHashtagList = []
        if (postCheck.toJSON().hashtagPosts)
            finalHashtagList.push(...postCheck.toJSON().hashtagPosts.map(item => item.hashtag_name))
        if (hashTags)
            finalHashtagList.push(...hashTags)
        finalHashtagList = _.uniq(finalHashtagList)
        if(removeHashTags){
            finalHashtagList = finalHashtagList.filter(item => !removeHashTags.includes(item))
        }
        console.log('finalHashtagList',finalHashtagList)

        postBody.hashtags = finalHashtagList.join(',')

        const post = await Post.forge({ id: req.params.id }).save(postBody); //inserting post to post table

        //delete attachment
        if (deleteAttachments && deleteAttachments.length > 0) {
            await Attachment.where("id", "in", deleteAttachments).destroy({ require: false });
        }

        //Saving photopaths
        if (photoPaths && photoPaths.length > 0) {
            let attachments = [];
            photoPaths.forEach((photoPath) => {
                attachments.push({
                    entity_type: 'Post',
                    entity_id: post.id,
                    ...photoPath
                });
            });
            // Save photo paths
            await Attachment.collection(attachments).invokeThen('save');
        }

        //Saving hashtags
        console.log('hashTags', hashTags)
        if (hashTags && hashTags.length > 0) {
            let presentHashtags = postCheck.toJSON().hashtagPosts
            if (presentHashtags) {
                presentHashtags = presentHashtags.map(item => item.hashtag_name)
            }
            const postHashtagList = []
            hashTags.forEach(item => {
                if (!presentHashtags.includes(item)) {
                    const hashTagObj = {
                        hashtag_name: item,
                        post_id: post.id
                    }
                    postHashtagList.push(hashTagObj)
                }
            })
            //increment like count
            hashTags.forEach(async item => {
                const increment = await HashTag.query().where('name', '=', item).increment('count', 1);
                console.log(`increment for ${item} is ${increment}`)
            })
            if (postHashtagList.length > 0)
                await HashtagPost.collection(postHashtagList).invokeThen('save');
        }

        //Removing old hashtagls
        if (removeHashTags && removeHashTags.length > 0) {
            removeHashTags = _.uniq(removeHashTags)
            const postHashtagList = []
            let presentHashtags = postCheck.toJSON().hashtagPosts
            console.log('presentHashtags', presentHashtags)
            if (presentHashtags) {
                removeHashTags.forEach(item => {
                    const postHashtag = presentHashtags.find(postHashtagItem => postHashtagItem.hashtag_name == item)
                    if (postHashtag) {
                        postHashtagList.push({ id: postHashtag.id })
                    }
                })
                //decrement like count
                removeHashTags.forEach(async item => {
                    const decrement = await HashTag.query().where('name', '=', item).increment('count', 1);
                    console.log(`decrement for ${item} is ${decrement}`)
                })
                console.log('postHashtagList', postHashtagList)
                if (postHashtagList.length > 0) {
                    console.log('deleteing hashtag')
                    await HashtagPost.collection(postHashtagList).invokeThen('destroy');
                }
            }
        }


        const postRes = await Post.where({ id: post.id }).fetch({
            require: false,
            withRelated: [{ 'user': query => query.column(constants.userFeedFields) }, 'attachments', 'hashtagPosts', 'activity', {'reposts': query => query.column('id', 'parent_id', 'user_id').where({user_id: req.user.id})}]
        });

        return res.success({ post: postRes });
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
