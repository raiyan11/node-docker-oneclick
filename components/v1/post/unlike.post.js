'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const Like = require('../../../models/like');
const Activity = require('../../../models/activity');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');

module.exports = async (req, res, next) => {
    try {

        var postId = req.params.id;

        let post = await Post.where({ id: postId }).fetch({ require: false });
        if (!post)
            return res.serverError(400, ErrorHandler(constants.post.notFound));

        // same user cannot dislike the same post
        let dislikeChk = await Like.where({ post_id: postId, user_id: req.user.id }).fetch({ require: false });

        if (!dislikeChk)
            return res.serverError(400, ErrorHandler(constants.post.alreadyDislikedByUser));

        await dislikeChk.destroy();

        //  decrement count in Activity tbl
        await Activity.query().where('post_id', postId).decrement('likes_count', 1);

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
