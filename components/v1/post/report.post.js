'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const Report = require('../../../models/report');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');

module.exports = async (req, res, next) => {
    try {

        let postCheck = await Post.where({ id: req.params.id }).fetch({ require: false });
        if (!postCheck)
            return res.serverError(400, ErrorHandler(constants.post.notFound));


        let postUserReportCheck = await Report.where({ post_id: req.params.id, user_id: req.user.id }).fetch({ require: false });
        if (postUserReportCheck)
            return res.serverError(400, ErrorHandler(constants.post.alreadyReportedByUser));

        var reportObj = {
            user_id: req.user.id,
            post_id: req.params.id
        }
        await Report.forge(reportObj).save();
        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
