'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const Hide = require('../../../models/hide');
const HashtagPost = require('../../../models/hashtag.post');
const HashtagUser = require('../../../models/hashtag.user');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');

module.exports = async (req, res, next) => {
    try {

        var postId = req.params.id;

        let postChk = await Post.where({ id: postId }).fetch({ require: false, withRelated: ['hashtagPosts'] });
        if (!postChk)
            return res.serverError(400, ErrorHandler(constants.post.notFound));
        let hide = await Hide.where({ user_id: req.user.id, post_id: postId }).fetch({ require: false });
        if (!hide)
            await new Hide().save({ user_id: req.user.id, post_id: postId });
        if (postChk.toJSON().parent_id) {
            let parentHide = await Hide.where({ user_id: req.user.id, post_id: postChk.toJSON().parent_id }).fetch({ require: false });
            if (!parentHide)
                await new Hide().save({ user_id: req.user.id, post_id: postChk.toJSON().parent_id });
        }


        const hashTagPosts = postChk.toJSON().hashtagPosts
        if (hashTagPosts && hashTagPosts.length > 0) {
            const hashTagNames = hashTagPosts.map(item => item.hashtag_name)

            let alreadyUserHashTags = await HashtagUser.where('hashtag_name', 'IN', hashTagNames).fetchAll({ require: false })
            if (alreadyUserHashTags)
                alreadyUserHashTags = alreadyUserHashTags.toJSON()
            else
                alreadyUserHashTags = []
            const postHashtagList = []
            hashTagPosts.forEach(item => {
                const hashTagObj = {
                    hashtag_name: item.hashtag_name,
                    user_id: req.user.id,
                    show: false,
                    active_status: constants.activeStatus.active
                }
                const userHashTagItem = alreadyUserHashTags.find(userHashItem => userHashItem.hashtag_name == item.hashtag_name)
                if (userHashTagItem) {
                    hashTagObj.id = userHashTagItem.id
                }
                postHashtagList.push(hashTagObj)
            })
            await HashtagUser.collection(postHashtagList).invokeThen('save');
        }

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
