
'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const constants = require('../../../config/constants');
const Hashtag = require('../../../models/hashtag');
const User = require("../../../models/user");

module.exports = async (req, res, next) => {
    try {


        const query = Hashtag.where({ approved: true, active_status: constants.activeStatus.active })
        const countQuery =  Hashtag.where({ approved: true, active_status: constants.activeStatus.active })

        if(req.query.name){
            query.where('name','ILIKE', "%" + req.query.name + "%")
            countQuery.where('name','ILIKE', "%" + req.query.name + "%")
        }

        const hashtags = await query.orderBy("count", "DESC").fetchPage({
            offset: (req.query.page - 1) * req.query.limit,
            limit: req.query.limit
        });

        const count = await countQuery.count();
        return res.success({ hashtags , count});

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}