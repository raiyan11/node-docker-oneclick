'use strict';
const {ErrorHandler, awsUtil} = require('../../../lib/utils');

module.exports = async (req, res) => {
    try {
        //Get signed url from aws
        const user = req.user;
        // const signedUrLs = await awsUtil.presignUrls(user.id, req.query.ext, req.query.count);
        const user_random = Math.floor(1000 + Math.random() * 9000);
      
        const signedUrLs = await awsUtil.presignarryOfUrl(user_random,req.body.ext);
       //Structure response
        const urls = [];
        signedUrLs.forEach(signedUrl => {
            urls.push({presigned_url: signedUrl.signedUrL, photo_path: signedUrl.publicDbUrl});
        });
        return res.success({urls});
       } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};