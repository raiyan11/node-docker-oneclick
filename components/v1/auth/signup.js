'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const User = require('../../../models/user');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');
const Session = require('../../../models/session');

module.exports = async (req, res, next) => {
    try {

        // Checking if email already exists. To speed up fetch we are just getting the count instead of the whole object
        const email = req.body.user.email.toLowerCase();
        const userName = req.body.user.user_name;
        const sameNameCount = await User.where({ user_name: userName }).count();
        const sameEmailCount = await User.where({ email: email }).count();
        console.log(`sameEmailCount - ${sameEmailCount}`);
        if (sameEmailCount > 0) {
            return res.serverError(422, ErrorHandler(new Error(constants.error.auth.emailTaken)));
        }
        if (sameNameCount > 0) {
            return res.serverError(422, ErrorHandler(new Error(constants.error.auth.userNameTaken)));
        }

        // Create a new user with date sent from api
        let sessionInfo = req.body.user.session_info;
        delete req.body.user.session_info;
        const body = req.body.user;
        const user = await User.forge(body).save();
     

        if (sessionInfo) {
            sessionInfo.user_id=user.id
            sessionInfo.last_active = new Date();

            await Session.forge().save(sessionInfo);
        }
    

        //Removed verify mail as it is not required for this app
        /*const otp = user.attributes.verification_token;
         console.log(otp);
         await sendGrid.send('Lips - verify User', 'verify-user', { name: `${user.toJSON().user_name}`, otp }, [email]);*/

        //Generate access tokens
        const userJSON =  user.toJSON()
        userJSON.approval_status = constants.approvalStatus.notSubmitted
        const data = await passportMiddleWare.generateSignUpToken(userJSON);
        return res.success(data);
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
