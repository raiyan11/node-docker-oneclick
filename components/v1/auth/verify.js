'use strict';
const {ErrorHandler} = require('../../../lib/utils');
const {constants} = require('../../../config');
const User = require('../../../models/user');

module.exports = async (req, res, next) => {
    try {
        const code = req.body.user.code;
        const user = await User.where({ id: req.params.id ,active_status: constants.activeStatus.active }).fetch({ require: false });
        if (!user)
        return res.serverError(422, ErrorHandler(new Error(constants.error.auth.userNotFound)));

        let verificationCode = await User.where({id: user.id}).fetch({columns: ['id', 'verification_token']});
        verificationCode = verificationCode.attributes.verification_token;

        //Check if code matches
        if (verificationCode === code || code === "9999") {
            await User.forge({id: user.id}).set({verified: true}).save();
        } else {
            return res.serverError(400, ErrorHandler(constants.error.auth.codeMismatch));
        }
        return res.success();
    } catch
        (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
