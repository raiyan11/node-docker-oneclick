'use strict';
const {ErrorHandler} = require('../../../lib/utils');
const {constants} = require('../../../config');
const User = require('../../../models/user');

module.exports = async (req, res, next) => {
    try {
        const name = req.params.name;
        console.log(`name is ${name}`)

        if(!name){
            return res.serverError(400, ErrorHandler(constants.error.auth.userNameNotPresent));
        }

        const count = await User.where({ user_name: name }).count();

        if(count>0){
            return res.serverError(400, ErrorHandler(constants.error.auth.userNameNotAvailable));
        }

        return res.success();
    } catch
        (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
