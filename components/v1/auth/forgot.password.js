"use strict";
const { ErrorHandler } = require("../../../lib/utils");
const User = require("../../../models/user");
const uuidv4 = require("uuid/v4");
const moment = require("moment");
const sendGrid = require("../../../lib/utils/sendgrid");
const constants = require("../../../config/constants");
// const dynamics = require("../../../lib/utils/firebase.dynamics");


module.exports = async (req, res) => {
    try {
        let user = await User.where({ email: req.body.user.email.toLowerCase() }).fetch({
            require: false,
        });
       // console.log(user);
        if (!user) {
            return res.serverError(
                400,
                ErrorHandler(new Error(constants.error.auth.userNotFound))
            );
        }

        user.reset_password_token = uuidv4();
        console.log(user.reset_password_token);
        user.reset_password_sent_at = moment.utc().format();
       
        let shortLink = `${global_Vault_key.url}/reset_password?reset_password_token=${user.reset_password_token}`;

        // const shortLink = await dynamics.generateDynamicLink("reset_token", user.reset_password_token)
        // console.log(shortLink);
        await sendGrid.send('Lips - Reset Your Password', 'forgot-password', { name: `${user.toJSON().user_name}`,shortLink}, [req.body.user.email]);

        await User.forge({ id: user.id }).save({
            reset_password_token: user.reset_password_token,
            reset_password_sent_at: user.reset_password_sent_at
        });
        return res.success();

    } catch (error) {
        console.log(error);
        return res.serverError(500, ErrorHandler(error));
    }
};
