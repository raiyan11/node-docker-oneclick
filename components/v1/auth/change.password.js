'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const User = require('../../../models/user');
const bcrypt = require('bcrypt');
const { bcryptConfig } = require('../../../config');
const constants = require('../../../config/constants');

//updating password with user access
module.exports = async (req, res, next) => {
    try {

        let body = req.body.user;
        console.log(req.user.email);
        //To get password we need to fetch from db again as it should'nt be exposed
        const user = await User.where({ id: req.user.id }).fetch({ columns: ['id', 'password'] });
        const currentPassword = user.attributes.password;
        console.log(currentPassword)

        if (bcrypt.compareSync(body.current_password, currentPassword)) {
            if (body.password === body.password_confirmation) {
                await User.forge({ id: req.user.id }).save({ password: body.password });
                return res.success();
            } else {
                return res.serverError(400, ErrorHandler(new Error(constants.error.auth.passwordNotMatch)));
            }
        } else {
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.passwordWrong)));
        }
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};