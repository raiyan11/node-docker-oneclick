'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const passport = require('passport');
const User = require('../../../models/user');
const Session = require('../../../models/session');
const { constants } = require('../../../config');
const { constant } = require('lodash');


module.exports = async (req, res, next) => {
    try {

        console.log('Calling local auth');
        //Local strategy auth using passport
        passport.authenticate('local', async (err, data, info) => {
            if (err) {
                console.log('err - ');
                console.log(err.message);
                return res.serverError(400, ErrorHandler(err));
            }
            let sessionInfo = req.body.user.session_info;

            if (sessionInfo) {
                let sessionDb = await Session.where({ user_id: data.user.id, active_status: constants.activeStatus.active }).fetch({ require: false });
                if (sessionDb)
                    await Session.forge({ id: sessionDb.id }).save({ active_status: constants.activeStatus.deleted });
                sessionInfo.user_id = data.user.id;
                sessionInfo.last_active = new Date();
                await Session.forge().save(sessionInfo);
            }

            return res.success(data);
        })(req, res);
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
