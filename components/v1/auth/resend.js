'use strict';
const { ErrorHandler, } = require('../../../lib/utils');
const User = require('../../../models/user');
const { constants } = require('../../../config');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const sendGrid = require("../../../lib/utils/sendgrid");

module.exports = async (req, res, next) => {
    try {
        console.log('resending code');
        const user = await User.where({id: req.params.id}).fetch();
        const otp = Math.floor(1000 + Math.random() * 9000);
        await sendGrid.send('Lips- Resend verify User', 'verify-user', {name: `${user.toJSON().user_name}`, otp}, [user.toJSON().email]);
        await User.forge({id: user.toJSON().id}).save({verified:false});
        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
