'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const constants = require('../../../config/constants');
const Follows = require('../../../models/follow');
const Notification = require('../../../models/notification');
const User = require("../../../models/user");

module.exports = async (req, res, next) => {
    try {

        let follow= await Follows.where({ follower_id: req.user.id, followee_id: req.params.id, active_status: constants.activeStatus.active }).fetch({require:false});
        if (!follow)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.Followuser)));

        await Follows.forge({ id: follow.id }).save({ active_status: constants.activeStatus.deleted });

        const notification = await Notification.where({ 'entity_id': follow.id, 'entity_type': 'Follow', type: 'requested_follow' }).fetch({ require: false })
        if (notification) {
            await notification.save({ active_status: constants.activeStatus.deleted })
        }

        return res.success();

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}