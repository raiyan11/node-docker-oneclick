'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const moment = require('moment');
const constants = require('../../../config/constants');
const Follow = require('../../../models/follow');
const Notification = require('../../../models/notification');
const User = require('../../../models/user');
const NotificationService = require('../../../services/notification.service');

module.exports = async (req, res, next) => {
    try {
        const user = req.user;
        let follow = await Follow.where({ id: req.params.id, followee_id: user.id, status: constants.requestStatus.requested }).fetch({ require: false });
        if (!follow)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.followNotFound)))

        await follow.save({ status: constants.requestStatus.accepted });

        const notification = await Notification.where({'entity_id':follow.id, 'entity_type': 'Follow', type:'requested_follow'}).fetch({require:false})
        if(notification){
            await notification.save({active_status:constants.activeStatus.deleted})
        }

        //send notification
        NotificationService.createNotification(
            follow.id, 'Follow', `Follow request accepted`, `${req.user.user_name} has accepted your follow request`, `accepted_follow`, follow.attributes.follower_id);

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}