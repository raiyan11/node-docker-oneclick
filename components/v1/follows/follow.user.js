'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const moment = require('moment');
const constants = require('../../../config/constants');
const Follows = require('../../../models/follow');
const User = require('../../../models/user');
const NotificationService = require('../../../services/notification.service');

module.exports = async (req, res, next) => {
    try {
        const user = req.user;
        const followUser = await User.where({ id: req.params.id }).fetch({ require: false });

        // self user check
        if ((user.id).toString() === req.params.id)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.sameUser)));

        if (!followUser)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.userNotFound)));

        // check if already following or requested
        const existsFollow = await Follows.where({ followee_id: req.params.id, follower_id: user.id, active_status: constants.activeStatus.active }).fetch({ require: false });
        if (existsFollow) {
            const existsFollowJSON = existsFollow.toJSON()
            if (existsFollowJSON.status == constants.requestStatus.requested)
                return res.serverError(400, ErrorHandler(new Error(constants.error.auth.requestAlreadySent)));
            else if (existsFollowJSON.status == constants.requestStatus.accepted)
                return res.serverError(400, ErrorHandler(new Error(constants.error.auth.requestAlreadyAccepted)));
            else if (existsFollowJSON.status == constants.requestStatus.denied) {

                let update;
                if (followUser.attributes.privacy_settings == 'public') {
                    update = await existsFollow.save({ status: constants.requestStatus.accepted });
                    NotificationService.createNotification(
                        update.attributes.id, 'Follow', `Follow started`, `${req.user.user_name} has started following you`, `started_follow`, req.params.id);
                } else {
                    update = await existsFollow.save({ status: constants.requestStatus.requested });
                    //send notification
                    NotificationService.createNotification(
                        update.attributes.id, 'Follow', `Follow request sent`, `${req.user.user_name} requested to follow you`, `requested_follow`, req.params.id);
                }
                return res.success({ follow: update });

            }

        }

        let follow;
        if (followUser.attributes.privacy_settings == 'public') {
            follow = await Follows.forge().save({ followee_id: req.params.id, follower_id: user.id, status: constants.requestStatus.accepted });
            NotificationService.createNotification(
                follow.attributes.id, 'Follow', `Follow started`, `${req.user.user_name} has started following you`, `started_follow`, req.params.id);
        } else {
            follow = await Follows.forge().save({ followee_id: req.params.id, follower_id: user.id });
            //send notification
            NotificationService.createNotification(
                follow.attributes.id, 'Follow', `Follow request sent`, `${req.user.user_name} requested to follow you`, `requested_follow`, req.params.id);
        }

        return res.success({ follow });

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}
