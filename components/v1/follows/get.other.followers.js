'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const moment = require('moment');
const constants = require('../../../config/constants');
const Follows = require('../../../models/follow');
const User = require('../../../models/user');

module.exports = async (req, res, next) => {
    try {

        const userId = req.params.id
        const requestedUser = await User.where({ id: userId }).count();

        if (requestedUser == 0)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.userNotFound)));
        // get the list of followers
        const followers = await Follows.where({ followee_id: userId })
            .where('status', '=', constants.requestStatus.accepted)
            .where('active_status', '=', constants.activeStatus.active)
            .fetchPage({
                offset: (req.query.page - 1) * req.query.limit,
                limit: req.query.limit,
                withRelated: [{'follower' : query=>  query.column(constants.userFollowFields)}]
            });

        const count = await Follows.where({ followee_id: userId })
        .where('status', '=', constants.requestStatus.accepted)
        .where('active_status', '=', constants.activeStatus.active).count();

        return res.success({ followers: followers, totalCount: count })
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}