'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const moment = require('moment');
const constants = require('../../../config/constants');
const Follow = require('../../../models/follow');
const Notification = require('../../../models/notification');

module.exports = async (req, res, next) => {
    try {
        const user = req.user;
        let follow = await Follow.where({ id: req.params.id, followee_id: user.id, status: constants.requestStatus.requested }).fetch({ require: false });
        if (!follow)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.followNotFound)));

        const notification = await Notification.where({ 'entity_id': follow.id, 'entity_type': 'Follow', type: 'requested_follow' }).fetch({ require: false })
        if (notification) {
            await notification.save({ active_status: constants.activeStatus.deleted })
        }

        follow = await Follow.forge({ id: req.params.id }).save({ status: constants.requestStatus.denied });

        return res.success({ follow });

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}