'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const moment = require('moment');
const constants = require('../../../config/constants');
const Follows = require('../../../models/follow');
const User = require('../../../models/user');

module.exports = async (req, res, next) => {
    try {
        const user = req.user;
        console.log(req.user.id)
        // get the list of followers
        const followers = await Follows.where({
            followee_id: user.id, active_status: constants.activeStatus.active})
            .where('status', '!=', constants.requestStatus.denied)
            .fetchPage({
                offset: (req.query.page - 1) * req.query.limit,
                limit: req.query.limit,
                withRelated: [{ 'follower': query => query.column(constants.userFollowFields) }]
            });

        const count = await Follows.where({ followee_id: user.id }).where('status', '!=', constants.requestStatus.denied)
            .where('active_status', '=', constants.activeStatus.active).count();

        return res.success({ followers: followers, totalCount: count })
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}