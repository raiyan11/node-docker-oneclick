'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const User = require('../../../models/user');
const moment = require('moment');
const constants = require('../../../config/constants');

module.exports = async (req, res, next) => {
  try {
    //Get logged in user
    const body = req.body.user;
    if (body.user_name) {
      const sameNameCount = await User.where({ user_name: body.user_name }).count();
      if (sameNameCount > 0) {
        return res.serverError(422, ErrorHandler(new Error(constants.error.auth.userNameTaken)));
      }
    }
    let userCheck = await User.where({ id: req.user.id }).fetch({ require: false });
    if (!userCheck)
      return res.serverError(400, ErrorHandler(new Error(constants.error.auth.userNotFound)));

    await User.forge({ id: req.user.id }).save(body, { patch: true });

    const user = await User.where({ id: req.user.id, active_status: constants.activeStatus.active }).fetch({ require: false, withRelated: [{ 'approval': query => query.select('status', 'user_id')},{ 'followers': query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }).select('id','followee_id', 'status', 'active_status') }, { 'following': query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }).select('id','follower_id', 'status', 'active_status') }] });

    return res.success({ user });
  } catch (error) {
    return res.serverError(500, ErrorHandler(error));
  }
};
