

'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Approval = require('../../../models/approval');
const User = require('../../../models/user');
const Attachment = require('../../../models/attachment');

module.exports = async (req, res, next) => {
    try {
        const body = req.body.approval;
        const photo_paths = body.photo_paths;
        delete body.photo_paths;

        body.user_id = req.user.id;

        console.log('req.user.id',req.user.id)
        let approval = await Approval.query({
            where: { user_id: req.user.id },
            andWhere: { active_status: constants.activeStatus.active }
        }).fetch({ require: false });

        console.log('approval',approval)

        if (approval) {
            const status = approval.toJSON().status
            console.log(`status is ${status}`)
            switch (status) {
                case constants.approvalStatus.accepted:
                    return res.serverError(400, ErrorHandler(constants.error.auth.approvalAccepted));
                case constants.approvalStatus.denied:
                    return res.serverError(400, ErrorHandler(constants.error.auth.approvalDenied)); 
                default:
                    return res.serverError(400, ErrorHandler(constants.error.auth.approvalRequested));
            }
        }

        let approvalSaveDb = await new Approval().save(body);


        if (photo_paths && photo_paths.length > 0) {
            let attachments = [];
            photo_paths.map((photoPath) => {
                attachments.push({
                    entity_type: 'Approve',
                    entity_id: approvalSaveDb.id,
                    ...photoPath
                });
            });
            // save photo paths
            await Attachment.collection(attachments).invokeThen('save');
        }

        return res.success();

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
