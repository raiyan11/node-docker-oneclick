'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const User = require('../../../models/user');
const Follow = require('../../../models/follow');
const Post = require('../../../models/post');
const moment = require('moment');
const constants = require('../../../config/constants');

module.exports = async (req, res, next) => {
    try {
        //Get logged in user
        let user = req.user
        if (!user)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.userNotFound)));
        await User.forge({ id: req.user.id }).save({ active_status: constants.activeStatus.inactive });

        // make follows of user to hidden
        let followData = await Follow.query({
            where: { followee_id: req.user.id },
            orWhere: { follower_id: req.user.id },
            andWhere: { active_status: constants.activeStatus.active }
        }).fetchAll({ require: false, columns: ['id'] })
        followData = followData.toJSON().map(follow => {
            const obj = { id: follow.id, active_status: constants.activeStatus.hidden }
            return obj
        })
        await Follow.collection(followData).invokeThen('save');

        // make posts of user to hidden
        let postData = await Post.query({
            where: { user_id: req.user.id, active_status: constants.activeStatus.active },
        }).fetchAll({ require: false, columns: ['id'] })
        postData = postData.toJSON().map(post => {
            const obj1 = { id: post.id, active_status: constants.activeStatus.hidden }
            return obj1
        })
        await Post.collection(postData).invokeThen('save');

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
