'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const User = require('../../../models/user');
const moment = require('moment');
const constants = require('../../../config/constants');

module.exports = async (req, res, next) => {
    try {
       
        let user = await User.where({ user_name: req.params.name, active_status: constants.activeStatus.active }).fetch({ require: false, withRelated: [{ 'blocked': query => query.where({ active_status: constants.activeStatus.active }) }, { 'followers': query => query.where({ active_status: constants.activeStatus.active }).select('id', 'followee_id', 'follower_id', 'status', 'active_status') }, { 'following': query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }).select('id',  'followee_id', 'follower_id', 'status', 'active_status') }] });
        const userAuth = req.user;
        
        if (!user)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.invalidUser)));

        let userJson = user.toJSON();
        if (!userAuth) {
            let show_post = false

            if (userJson.privacy_settings == constants.privacySetting.public) {
                show_post = true
            } 
            userJson.show_post = show_post

            return res.success({ user: userJson }); 
        }
      
        //IF the other user has blocked this user we shouldnt show details
        console.log('blocked', user.toJSON().blocked)
        const blocked = userJson.blocked

        const isBlocked = blocked.find(item => item.blocked_user_id == req.user.id)

        if (isBlocked) {
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.blockedByUser)));
        }

        delete userJson.blocked

        let show_post = false

        if (userJson.privacy_settings == constants.privacySetting.public) {
            show_post = true
        } 
        
        if (user.attributes.followers) {
            const amIfollower = user.attributes.followers.find(item => item.follower_id == req.user.id)
            console.log('amIfollower',amIfollower)

            if (!show_post && amIfollower &&  amIfollower.status == constants.requestStatus.accepted)
                show_post = true

            if(amIfollower){
                userJson.follow_status = amIfollower.status
            }else{
                userJson.follow_status = constants.followStatus.not_requested
            }

        }else{
            userJson.follow_status = constants.followStatus.not_requested
        }

        userJson.show_post = show_post

        return res.success({ user: userJson });
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
