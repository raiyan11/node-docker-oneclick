'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const User = require('../../../models/user');
const moment = require('moment');
const constants = require('../../../config/constants');

module.exports = async (req, res, next) => {
  try {
    //Get logged in user
    let user = await User.where({ id: req.user.id, active_status: constants.activeStatus.active }).fetch({ require: false, withRelated: [{ 'approval': query => query.select('status', 'user_id') },{ 'followers': query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }).select('id','followee_id', 'status', 'active_status') }, { 'following': query => query.where({ active_status: constants.activeStatus.active, status: constants.requestStatus.accepted }).select('id','follower_id', 'status', 'active_status') }] });
    return res.success({ user });
  } catch (error) {
    return res.serverError(500, ErrorHandler(error));
  }
};
