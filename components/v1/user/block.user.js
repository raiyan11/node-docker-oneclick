'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const constants = require('../../../config/constants');
const Block = require('../../../models/block');
const User = require("../../../models/user");
const Follows = require('../../../models/follow');

module.exports = async (req, res, next) => {
    try {

        console.log(`${req.params.id} and ${req.user.id}`)
        if (req.params.id == req.user.id)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.sameUser)));

        let userCount = await User.where({ id: req.params.id }).count();
        if (userCount == 0)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.wrongBlockUser)));

        let block = await Block.where({ blocked_user_id: req.params.id, user_id: req.user.id, active_status: constants.activeStatus.active }).fetch({ require: false });


        if (block)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.userAlreadyBlocked)));

        if (!block)
            await Block.forge({ blocked_user_id: req.params.id, user_id: req.user.id }).save();


        let followee = await Follows.where({ follower_id: req.params.id, followee_id: req.user.id, active_status: constants.activeStatus.active }).fetch({ require: false });
        if (followee)
            await Follows.forge({ id: followee.id }).save({ active_status: constants.activeStatus.hidden });

        let follower = await Follows.where({ followee_id: req.params.id, follower_id: req.user.id, active_status: constants.activeStatus.active }).fetch({ require: false });
        if (follower)
            await Follows.forge({ id: follower.id }).save({ active_status: constants.activeStatus.hidden });


        return res.success();


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}