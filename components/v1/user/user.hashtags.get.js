'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const constants = require('../../../config/constants');
const HashtagUser = require('../../../models/hashtag.user');
const User = require("../../../models/user");

module.exports = async (req, res, next) => {
  try {

    let presentTags = await HashtagUser.where({ user_id: req.user.id , active_status: constants.activeStatus.active}).fetchAll({ require: false })
    presentTags = presentTags.toJSON()
    console.log('presentTags', presentTags)

    const show =[]
    const hide = []

    presentTags.forEach(hashtagUser => {
        if(hashtagUser.show){
            show.push(hashtagUser.hashtag_name)
        }else{
            hide.push(hashtagUser.hashtag_name)
        }
    })

    return res.success({hide,show})
  } catch (error) {
    return res.serverError(500, ErrorHandler(error));
  }
}