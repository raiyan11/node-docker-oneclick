'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const User = require('../../../models/user');
const Block = require('../../../models/block')
const moment = require('moment');
const constants = require('../../../config/constants');

module.exports = async (req, res, next) => {
    try {
        //Get logged in user
        let blocks = await Block.where({ user_id: req.user.id, active_status: constants.activeStatus.active }).fetchPage({
            offset: (req.query.page - 1) * req.query.limit,
            limit: req.query.limit, require: false, withRelated: [{ 'blockedUser': query => query.select(constants.userFeedFields) }]
        });


        blocks = blocks.toJSON();

        blocks = blocks.map(item=> item.blockedUser)
        const count = await Block.where({ user_id: req.user.id, active_status: constants.activeStatus.active }).count();
        return res.success({ users:blocks, count });
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};