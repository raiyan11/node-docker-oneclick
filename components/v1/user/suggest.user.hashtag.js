'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const constants = require('../../../config/constants');
const HashTag = require('../../../models/hashtag');
const User = require("../../../models/user");

module.exports = async (req, res, next) => {
    try {
        let body = req.body.hashtag;
        body.created_by = req.user.id;
        const hashDb = await HashTag.where({ name: body.name }).fetch({ require: false });
        if (hashDb)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.HashtagAlreadyExists)));

        let hashtag = await new HashTag(body).save();
        return res.success({ hashtag });


    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}