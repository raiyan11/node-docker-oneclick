'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const constants = require('../../../config/constants');
const HashtagUser = require('../../../models/hashtag.user');
const User = require("../../../models/user");

module.exports = async (req, res, next) => {
  try {
    let show = req.body.hashtags.show;
    let hide = req.body.hashtags.hide;
    let remove = req.body.hashtags.remove;

    const allHashtags = []

    if (show)
      allHashtags.push(...show)
    if (hide)
      allHashtags.push(...hide)
    if (remove)
      allHashtags.push(...remove)

    console.log('allHashtags', allHashtags)

    if (allHashtags.length == 0)
      return res.serverError(500, ErrorHandler(constants.error.auth.nothingToUpdate));

    let presentTags = await HashtagUser.where({ user_id: req.user.id }).where('hashtag_name', 'in', allHashtags).fetchAll({ require: false })
    presentTags = presentTags.toJSON()
    console.log('presentTags', presentTags)

    const needSave = []
    if (show) {
      show.forEach(hashtag => {
        const presentTag = presentTags.find(item => item.hashtag_name == hashtag)
        const updateObject = {}
        if (presentTag) {
          updateObject.id = presentTag.id
        }
        updateObject.user_id = req.user.id
        updateObject.hashtag_name = hashtag
        updateObject.show = true
        updateObject.active_status = constants.activeStatus.active
        needSave.push(updateObject)
      })
    }
    if (hide) {
      hide.forEach(hashtag => {
        const presentTag = presentTags.find(item => item.hashtag_name == hashtag)
        const updateObject = {}
        if (presentTag) {
          updateObject.id = presentTag.id
        }
        updateObject.user_id = req.user.id
        updateObject.hashtag_name = hashtag
        updateObject.show = false
        updateObject.active_status = constants.activeStatus.active
        needSave.push(updateObject)
      })
    }
    if (remove) {
      remove.forEach(hashtag => {
        const presentTag = presentTags.find(item => item.hashtag_name == hashtag)
        const updateObject = {}
        if (presentTag) {
          updateObject.id = presentTag.id
          updateObject.active_status = constants.activeStatus.deleted
          needSave.push(updateObject)
        }

      })
    }

    console.log('needSave', needSave)
    await HashtagUser.collection(needSave).invokeThen('save')
    return res.success({})



  } catch (error) {
    return res.serverError(500, ErrorHandler(error));
  }
}