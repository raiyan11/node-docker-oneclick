'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Post = require('../../../models/post');
const HashTagPost = require('../../../models/hashtag.post');
const passportMiddleWare = require('../../../middlewares/passport.middleware');
const moment = require('moment');

module.exports = async (req, res, next) => {
  try {
    let post;
    if (req.query.title_name) {
      post = await Post
        .where({ user_id: req.user.id })
        .where('title', 'ILIKE', "%" + req.query.title_name + "%").fetch({ require: false })
        .orderBy('created_at', req.query.order_by)
        .fetchPage({
          offset: (req.query.page - 1) * req.query.limit,
          limit: req.query.limit,
          withRelated: [{ 'user': query => query.column(constants.userFeedFields) }]
        });
    }
    else {
      post = await Post
        .where({ user_id: req.user.id })
        .orderBy('created_at', req.query.order_by)
        .fetchPage({
          offset: (req.query.page - 1) * req.query.limit,
          limit: req.query.limit,
          withRelated: [{ 'user': query => query.column(constants.userFeedFields) }]
        });
    }
    return res.success({ post });
  } catch (error) {
    return res.serverError(500, ErrorHandler(error));
  }
};
