'use strict';
const { ErrorHandler, awsUtil } = require('../../../lib/utils');
const User = require('../../../models/user');
const Follow = require('../../../models/follow');
const moment = require('moment');
const constants = require('../../../config/constants');

module.exports = async (req, res, next) => {
    try {
        //Get logged in user
        let user = await User.where({ id: req.user.id }).fetch({ require: false });
        if (!user)
            return res.serverError(400, ErrorHandler(new Error(constants.error.auth.userNotFound)));


        await User.forge({ id: req.user.id }).save({ active_status: constants.activeStatus.deleted });

        let followData = await Follow.query({
            where: { followee_id: req.user.id },
            orWhere: { follower_id: req.user.id }
        }).fetchAll({ require: false, columns: ['id'] })

        console.log('followData before map', followData.toJSON())

        followData = followData.toJSON().map(follow => {
            const obj = { id: follow.id, active_status: constants.activeStatus.deleted }
            follow.id
            return obj
        })
        console.log('followData after map', followData)

        await Follow.collection(followData).invokeThen('save');

        return res.success();
    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};
