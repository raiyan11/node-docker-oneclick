'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const { constants } = require('../../../config');
const Approval = require('../../../models/approval');
const User = require('../../../models/user');
const Code = require('../../../models/codes');
const moment = require("moment");

module.exports = async (req, res, next) => {
    try {
        const body = req.body;
        const user = req.user;
        console.log(moment().format());
        const codedb = await Code.where({ code: body.code, email: user.email })
            .where('expires_at', '>', moment().format()).fetch({ require: false })
        console.log(codedb);
        if (!codedb)
            return res.serverError(400, ErrorHandler(constants.error.auth.codeNotFound));

        // check the user is already approved
        const approval = await Approval.where({ user_id: user.id }).fetch({ require: false })
        if (approval) {
            if (approval.toJSON().status == constants.approvalStatus.accepted)
                return res.serverError(400, ErrorHandler(constants.error.auth.approvalAccepted));
            else
                await approval.save({ status: constants.approvalStatus.accepted, own_content: true })
        } else
            await Approval.forge().save({ user_id: user.id, status: constants.approvalStatus.accepted, own_content: true })

        return res.success();

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
}