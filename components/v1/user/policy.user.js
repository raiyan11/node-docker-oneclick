'use strict';
const { ErrorHandler } = require('../../../lib/utils');
const constants = require('../../../config/constants');
const User = require("../../../models/user");

module.exports = async (req, res, next) => {
    try {
        const body = req.body

        await User.forge({ id: req.user.id }).save(body);

        return res.success();


    } catch (error) {
        console.log('error', error)
        return res.serverError(500, ErrorHandler(error));
    }
}