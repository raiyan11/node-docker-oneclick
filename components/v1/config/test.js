'use strict';
const {ErrorHandler, awsUtil} = require('../../../lib/utils');

module.exports = async (req, res) => {
    try {
        
        const dimensions = await awsUtil.s3GetSize('uploads/user/6909/localhost_1604570902285.png',false)

        return res.success({dimensions});

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};