'use strict';
const {ErrorHandler, VaultKey} = require('../../../lib/utils');

module.exports = async (req, res) => {
    try {
   //envKey
   //envKeysets
      let forceUpdate = await  VaultKey.envKeysets()
        return res.success({ global_Vault_key});

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};