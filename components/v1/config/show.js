'use strict';
const {ErrorHandler, awsUtil,VaultKey} = require('../../../lib/utils');

module.exports = async (req, res) => {
    try {
     let forceUpdate = false;
        let updateAvailable = false;
        if (req.query.hasOwnProperty("ios_version")) {
          if (req.query.ios_version < global_Vault_key.iosMin) {
            forceUpdate = true;
          }
          if (req.query.ios_version <global_Vault_key.iosMax) {
            updateAvailable = true;
          }
        } else if (req.query.hasOwnProperty("android_version")) {
          if (req.query.android_version <global_Vault_key.androidMin) {
            forceUpdate = true;
          }
          if (req.query.android_version < global_Vault_key.androidMax) {
            updateAvailable = true;
          }
        }
       // let forceUpdate = await  VaultKey.envKeysets()
   
        return res.success({ forceUpdate, updateAvailable, dbDetails:global_Vault_key});

    } catch (error) {
        return res.serverError(500, ErrorHandler(error));
    }
};