"use strict";
const { ErrorHandler } = require("../../../lib/utils");
const sendGrid = require("../../../lib/utils/sendgrid");
const constants = require("../../../config/constants");
// const dynamics = require("../../../lib/utils/firebase.dynamics");


module.exports = async (req, res) => {
    try{
        await sendGrid.send('Lips - Contact Us', 'contact-us', req.body, ['testqa@bitcot.com']);
    
        return res.success();

    } catch (error) {
        console.log(error);
        return res.serverError(500, ErrorHandler(error));
    }
};
